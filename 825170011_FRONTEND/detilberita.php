<?php
include "include/config.php";
$berita = $_GET['detilberita'];
$ambil = mysqli_query($connection, "SELECT * from berita where kategoriberitaKODE='$berita' ");
$row = mysqli_fetch_array($ambil);

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Berita</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/cssgalerii.css" rel="stylesheet">
</head>

<body>
    <?php
    include("include/menu.php");
    ?>
    <div class="container">
        <div class="jumbotron" style="text-align:center">
            <p>Berita</p>
        </div>

        <div class="row">
            <div class="col-sm-8">
                <div class="media">
                    <div class="media-left">
                        <img src="imagesuk/<?php echo $row['beritaICONFOTO'] ?>" style="margin-top:30%" width="200px;" height="200px;">
                        </a>
                    </div>
                    <div class="media-body">
                        <h1><?php echo $row['beritaJUDUL']; ?></h1>
                        <p>Kategori berita KODE :<?php echo $row['kategoriberitaKODE']; ?></p>
                        <br>
                        <p>Berita ISI :<?php echo $row['beritaISI']; ?></p>
                        <p>Event KODE:<?php echo $row['eventKODE']; ?></p>
                        <p>Kabupaten KODE:<?php echo $row['kabupatenKODE']; ?></p>
                        <br>
                        <p>Berita ISI2:<?php echo $row['beritaISI2']; ?></p>
                        <br>
                        <p>Berita Tanggal: <?php echo $row['beritaTGL']; ?></p>
                        <br>
                        <p>Berita Sumber :<?php echo $row['beritaSUMBER']; ?></p>
                        <br>
                    </div>
                </div>
            </div>
        </div>

        <?php
        include("include/footer.php");
        ?>
    </div>



</body>

</html>