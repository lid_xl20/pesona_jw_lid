<?php
$ambilberita = mysqli_query($connection, "SELECT * from berita ");
$databerita = mysqli_fetch_array($ambilberita);
$ambilhotel = mysqli_query($connection, "SELECT *from hotel");
$datahotel = mysqli_fetch_array($ambilhotel);
?>

<div class="container">
    <div class="row">
        <div class="col-sm-8">
            <?php if (mysqli_num_rows($ambilberita) > 0) { ?>
                <?php while ($databerita = mysqli_fetch_array($ambilberita)) { ?>
                    <div class="media">
                        <div class="media-left">
                            <a href="#">
                                <img class="media-object" style="width:150px;height:180px" ; src="imagesuk/<?php echo $databerita['beritaICONFOTO']; ?>" alt="tidak ada">
                            </a>
                        </div>
                        <div class="media-body">

                            <h4 class="media-heading"><?php echo $databerita['beritaSUMBER']; ?></h4>
                            <h1><?php echo $databerita['beritaJUDUL']; ?></h1>
                            <!--https://bootsnipp.com/snippets/8yNX-->
                            <div class="down">
                                <p1><a class="btn btn-primary btn-lg" href="solo.php?kodeberita=<?php echo $databerita["beritaKODE"] ?> " role="button" style="font-size:13px">Read more </a></p1>
                            </div>

                        </div>

                    </div>
                <?php } ?>

            <?php } ?>
        </div>

        <div class="col-sm-4">
            <?php if (mysqli_num_rows($ambilhotel) > 0) { ?>
                <?php while ($datahotel = mysqli_fetch_array($ambilhotel)) { ?>
                    <div class="list-group">
                        <a href="#" class="list-group-item active">
                            <div class="media-left">
                                <img class="media-object" style="width:150px;height:120px" ; src="images/<?php echo $datahotel['hotelFOTO']; ?>" alt="tidak ada">
                            </div>
                            <div class="media-body">
                                <h4 class="list-group-item-heading"><?php echo $datahotel['hotelNAMA']; ?></h4>
                                <p><?php echo $datahotel['hotelKELAS']; ?>
                                </p>
                                <p><?php echo $datahotel['hotelFASILITAS']; ?>
                                </p>
                            </div>
                        </a>
                    </div>
                <?php } ?>
            <?php } ?>
        </div>

    </div>


</div>