<?php $result = $connection->query("SELECT * from SLIDER"); ?>

<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">

    <ol class="carousel-indicators">
        <?php
        $i = 0;
        foreach ($result as $row) {
            $actives = '';
            if ($i == 0) {
                $actives = 'active';
            }
            ?>
            <li data-target="#carousel-example-generic" data-slide-to="<?php echo $i; ?>" class="<?php echo $actives; ?>"></li>
        <?php $i++;
        } ?>
    </ol>
    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
        <?php
        $i = 0;
        foreach ($result as $row) {
            $actives = '';
            if ($i == 0) {
                $actives = 'active';
            }
            ?>
            <div class="item <?php echo $actives ?>">
                <!--ubah-->
                <img src="imagesuk/<?php echo $row['sliderFOTO'] ?>" alt="Gambar ga ada">
                <div class="carousel-caption">
                    <h1><?php echo $row['sliderJUDUL'] ?> </h1>
                    <h3> <?php echo $row['sliderKET'] ?></h3>
                </div>
            </div>
        <?php $i++;
        } ?>

        <!--untuk bagian previous < > next-->
    </div>
    <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>