<?php
include "include/config.php";
$querykabupaten = mysqli_query($connection, "SELECT * FROM kabupaten");
$querywisata = mysqli_query($connection, "SELECT * FROM kategoriwisata");
$querykecamatan = mysqli_query($connection, "SELECT *from kecamatan");
$queryberita = mysqli_query($connection, "SELECT *FROM kategoriberita");
$queryresto = mysqli_query($connection, "SELECT *FROM restoran");
$queryhotel = mysqli_query($connection, "SELECT *FROM hotel");
?>
<!--navbar dalam bootstrap-->
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <!-- <img src="imagesuk/pesonaind.jpg" style="width:150px;height:100px">-->
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>

      <a class="navbar-brand" href="#"><img src="imagesuk/pesona.png" style="width:120px;height:35px"></a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">

        <!--dari sini-->
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Kabupaten <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <!--untuk bagian yang dropdown kabupaten-->
            <?php if (mysqli_num_rows($querykabupaten) > 0) { ?>
              <!--rowkabupaten diinisialisasi sendiri-->
              <?php while ($rowkabupaten = mysqli_fetch_array($querykabupaten)) {
                  ?>
                <li>
                  <a href="infokabupaten.php?kodekab=<?php echo $rowkabupaten["kabupatenKODE"] ?>">
                    <?php echo $rowkabupaten["kabupatenNAMA"] ?>
                  </a>
                </li>
              <?php
                } ?>
            <?php   } ?>

          </ul>
        </li>

        <!--query wisata -->
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Destinasi <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <?php if (mysqli_num_rows($querywisata) > 0) { ?>
              <?php while ($rowwisata = mysqli_fetch_array($querywisata)) { ?>
                <li>
                  <a href="detilwisata.php?kodewisata=<?php echo $rowwisata["kategoriKODE"] ?>">
                    <?php echo $rowwisata["kategoriNAMA"] ?>
                  </a>
                </li>
              <?php
                } ?>
            <?php
            } ?>
          </ul>
        </li>


        <!--Berita-->
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Berita <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <!--untuk bagian yang dropdown kabupaten-->
            <?php if (mysqli_num_rows($queryberita) > 0) { ?>
              <!--rowkabupaten diinisialisasi sendiri-->
              <?php while ($rowberita = mysqli_fetch_array($queryberita)) {
                  ?>
                <li>
                  <a href="detilberita.php?detilberita=<?php echo $rowberita["kategoriberitaKODE"] ?>">
                    <?php echo $rowberita["kategoriberitaNAMA"] ?>
                  </a>
                </li>
              <?php
                } ?>
            <?php   } ?>

          </ul>
        </li>

        <!--penutup ul paling atas-->
      </ul>
      <form class="navbar-form navbar-left">
        <div class="form-group">
          <input type="text" class="form-control" placeholder="Search">
        </div>
        <button type="submit" class="btn btn-default">Submit</button>
      </form>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="#">
            <?php date_default_timezone_set("Asia/Jakarta");
            echo date(" l");
            $date = date(" d-m-o");
            echo $date;
            echo date(" h:i:s A");
            ?>
          </a>
        </li>

        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Tabel <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <?php $resto = mysqli_fetch_array($queryresto); ?>
            <?php $hotel = mysqli_fetch_array($queryhotel); ?>
            <li><a href="restodropdown.php?koderesto=<?php echo $resto['restoranKODE'] ?> ">Restoran</a></li>
            <li><a href="hoteldropdown.php?kodehotel=<?php echo $hotel['hotelKODE'] ?> ">Hotel</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="index.php">Index Awal</a></li>
            <li><a href="logout.php">Logout</a></li>
          </ul>
        </li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>