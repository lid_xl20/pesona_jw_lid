<?php
include "config.php";
$query = mysqli_query($connection, "SELECT * from kabupaten ");
?>
<div class="container mt40">
    <section class="row">
        <?php if (mysqli_num_rows($query) > 0) { ?>
            <?php while ($row = mysqli_fetch_array($query)) { ?>
                <article class="col-xs-12 col-sm-6 col-md-3">
                    <div class="panel panel-default" style="width:230px;height:200px;">
                        <div class="panel-body">
                            <a href="imagesuk/<?php echo $row["kabupatenFOTOICON"]; ?>" title="<?php echo $row["kabupatenNAMA"]; ?>" class="zoom" data-title="<?php echo $row["kabupatenNAMA"]; ?>" data-footer="<?php echo $row["kabupatenFOTOICONKET"]; ?>" data-type="image" data-toggle="lightbox">
                                <img src="imagesuk/<?php echo $row["kabupatenFOTOICON"]; ?>" style="width:230px;height:150px;">
                                <span class="overlay"><i class="glyphicon glyphicon-fullscreen"></i></span>
                            </a>
                        </div>
                        <div class="panel-footer" style="height:80px;">
                            <h4><a href="infokabupaten.php?kodekab=<?php echo $row["kabupatenKODE"] ?>" title="<?php echo $row["kabupatenNAMA"]; ?>"><?php echo $row["kabupatenNAMA"]; ?></a></h4>
                            <span class="pull-right">
                                <i id="like1" class="glyphicon glyphicon-thumbs-up"></i>
                                <div id="like1-bs3"></div>
                                <i id="dislike1" class="glyphicon glyphicon-thumbs-down"></i>
                                <div id="dislike1-bs3"></div>
                            </span>
                        </div>
                    </div>
                </article>

            <?php } ?>

        <?php } ?>

    </section>
</div>