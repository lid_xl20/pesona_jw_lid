<?php
include "include/config.php";
$ambil = mysqli_query($connection, "SELECT * from restoran ");
$data = mysqli_fetch_array($ambil);
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Restoran</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/cssgalerii.css" rel="stylesheet">
</head>

<body>
    <?php
    include("include/menu.php");
    ?>
    <div class="container">
        <div class="container">
            <div class="jumbotron" style="text-align:center">
                <p>Restoran</p>
            </div>
            <div class="row">
                <div class="col-sm-8">
                    <?php if (mysqli_num_rows($ambil) > 0) { ?>
                        <?php while ($data = mysqli_fetch_array($ambil)) { ?>
                            <div class="media">
                                <div class="media-left">
                                    <a href="#">
                                        <img class="media-object" src="images/<?php echo $data['restoranFOTO']; ?>" style="width:200px;height:220px;">
                                    </a>
                                </div>
                                <div class="media-body">
                                    <h4 class="media-heading"></h4>
                                    <h1><?php echo $data['restoranMenu']; ?></h1>
                                    <p>Harga Menu :<?php echo $data['RestoranHargaMenu'] ?></p>
                                    <p>Tanggal Pemesanan : <?php echo $data['RestoranTGL'] ?></p>
                                </div>
                            </div>

                        <?php } ?>
                    <?php } ?>
                </div>
            </div>
            <?php
            include("include/footer.php");
            ?>
        </div>



</body>

</html>