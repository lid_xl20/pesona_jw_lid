<?php
include "include/config.php";
$kab = $_GET['kodekab'];
$ambil = mysqli_query($connection, "SELECT * from kecamatan where kabupatenKODE='$kab' ");
$data = mysqli_fetch_array($ambil);
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Galeri Kecamatan</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/cssgalerii.css" rel="stylesheet">
</head>

<body>
    <?php
    include("include/menu.php");
    ?>
    <div class="container">
        <div class="jumbotron" style="text-align:center">
            <p>Kecamatan</p>
        </div>
        <div class="row">
            <div class="col-sm-8">
                <div class="media">
                    <div class="media-left">
                        <a href="wisata.php?kodekec=<?php echo $data['kecamatanKODE']; ?>">
                            <img src="imagesuk/<?php echo $data['kecamatanFOTO'] ?>" style="margin-top:30%" width="200px;" height="200px;">
                        </a>
                    </div>
                    <div class="media-body">
                        <h1><?php echo $data['kecamatanNAMA']; ?></h1>
                        <!--TAMBAHIN YANG DATA DI KABUPATEN-->
                        <p>KECAMATAN ALAMAT :<?php echo $data['kecamatanALAMAT']; ?></p>
                        <p>KECAMATAN KETERANGAN:<?php echo $data['kecamatanKET']; ?></p>
                        <p>TANGGAL:<?php echo $data['kecamatanTGL']; ?></p>
                    </div>
                </div>
            </div>
            <!--tambahin yg list group samping-->
            <div class="col-sm-4">
                <div class="list-group">
                    <a href="#" class="list-group-item active">
                        <h4 class="list-group-item-heading">List group Lake Ranau</h4>
                        <p>Lake Ranau Legend dance, which is very epic
                        </p>
                        <p class="list-group-item-text">Best</p>
                    </a>
                </div>
                <div class="list-group">
                    <a href="#" class="list-group-item active">
                        <h4 class="list-group-item-heading">List group Pengadangan</h4>
                        <p>Pengadangan, Ogan Traditional Marriage Tradition</p>
                        <p class="list-group-item-text">Best</p>
                    </a>
                </div>
                <div class="list-group">
                    <a href="#" class="list-group-item active">
                        <h4 class="list-group-item-heading">List group Kaisai Bathing</h4>
                        <p>The Kasai Bathing Tradition in Marriage in Lubuk Linggau </p>
                        <p class="list-group-item-text">Best</p>
                    </a>
                </div>
            </div>
        </div>
        <?php
        include("include/footer.php");
        ?>
    </div>

</body>

</html>