<?php
include "include/config.php";
$berita = $_GET['kodeberita'];
$ambilberita = mysqli_query($connection, "SELECT * from fotoberita where beritaKODE='$berita' ");
$databerita = mysqli_fetch_array($ambilberita);
?>



<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Berita Terbaru</title>
</head>

<body>
    <?php
    include "include/menu.php"  ?>
    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
            <li data-target="#carousel-example-generic" data-slide-to="2"></li>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
            <div class="item active">
                <!--ubah-->
                <img src="imagesuk/phuket2.jpg" alt="Gambar ga ada">
                <div class="carousel-caption">
                    <h1> Foto FantaSea</h1>
                    <h3> kawasan Semarang </h3>

                </div>
            </div>
            <div class="item">
                <img src="imagesuk/kamala.jpg" alt="Gambar ga ada">
                <div class="carousel-caption">
                    <h1> Foto Plantation Kamala Beach</h1>
                    <h3> kawasan terkenal di Tambon Kamala</h3>
                </div>
            </div>

            <div class="item">
                <img src="imagesuk/phuket.jpg" alt="Gambar ga ada">
                <div class="carousel-caption">
                    <h1> Foto Hotel Kathu </h1>
                    <h3> kawasan terkenal di Kamala </h3>
                </div>
            </div>
        </div>



        <!-- Controls -->
        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <link rel="stylesheet" type="text/css" href="css/thailand.css">

    <!------ Include the above in your HEAD tag ---------->

    <div class="container">

        <!--hello-->
        <div class="jumbotron" style="text-align:center;width:100%;">
            <h1><?php echo $databerita['beritafotoJUDUL'] ?></h1>
            <p>BERITA FOTO NAMA :<?php echo $databerita['beritafotoNAMA'] ?></p>
            <p><?php echo $databerita['beritafotoKET'] ?></p>
            <p><?php echo $databerita['beritafotoTGLAMBIL'] ?></p>
        </div>
        <div class="row">
            <div class="col-xs-8 col-xs-offset-2">
                <h4>Made by <a href="https://facebook.com/lidya.wang.9" class="btn btn-primary btn-xs" target="_blank"><i class="fa fa-fw fa-facebook"></i>@lidyawang</a></h4>

            </div>
        </div>
        <hr>
        <!-- Begin of rows -->
        <div class="row carousel-row">
            <div class="col-xs-8 col-xs-offset-2 slide-row">
                <div id="carousel-1" class="carousel slide slide-carousel" data-ride="carousel">
                    <!-- Indicators -->
                    <ol class="carousel-indicators">
                        <li data-target="#carousel-1" data-slide-to="0" class="active"></li>
                        <li data-target="#carousel-1" data-slide-to="1"></li>
                        <li data-target="#carousel-1" data-slide-to="2"></li>
                    </ol>

                    <!-- Wrapper for slides -->
                    <div class="carousel-inner">
                        <div class="item active">
                            <img src="https://image.cermati.com/q_70/gwqolssqyl6jdruoepkk.webp" alt="Image">
                        </div>
                        <div class="item">
                            <img src="https://image.cermati.com/q_70/cw0yzuldgovwunpq0mum.webp" alt="Image">
                        </div>
                        <div class="item">
                            <img src="https://image.cermati.com/q_70/i3buywosn4eh0ewk5fd7.webp" alt="Image">
                        </div>
                    </div>
                </div>
                <div class="slide-content">
                    <h4>What travelers are sating</h4>
                    <p>
                        If you want to visit the beautiful waterfall on Koh Samui.be sure to visit the waterfall on muang II
                    </p>
                </div>
                <div class="slide-footer">
                    <span class="pull-right buttons">
                        <button class="btn btn-sm btn-default"><i class="fa fa-fw fa-eye"></i> Show</button>
                        <button class="btn btn-sm btn-primary"><i class="fa fa-fw fa-shopping-cart"></i> Buy</button>
                    </span>
                </div>
            </div>
        </div>
        <div class="row carousel-row">
            <div class="col-xs-8 col-xs-offset-2 slide-row">
                <div id="carousel-2" class="carousel slide slide-carousel" data-ride="carousel">
                    <!-- Indicators -->
                    <ol class="carousel-indicators">
                        <li data-target="#carousel-2" data-slide-to="0" class="active"></li>
                        <li data-target="#carousel-2" data-slide-to="1"></li>
                        <li data-target="#carousel-2" data-slide-to="2"></li>
                    </ol>

                    <!-- Wrapper for slides -->
                    <div class="carousel-inner">
                        <div class="item active">
                            <img src="https://image.cermati.com/q_70/exyfzba2qt4od3i7u7qx.webp" alt="Image">
                        </div>
                        <div class="item">
                            <img src="https://image.cermati.com/q_70/qknosuw2knairck3pnp2.webp" alt="Image">
                        </div>
                        <div class="item">
                            <img src="https://image.cermati.com/q_70/roumq1in51sk4lz3lkdl.webp" alt="Image">
                        </div>
                    </div>
                </div>
                <div class="slide-content">
                    <h4>Interest in tour?</h4>
                    <p>

                        Solo, or ‘Purple Waterfalls’, are so named because of the striking purple shade of their rock faces.
                        There are other waterfalls to visit on Solo, but these are considered to be among the most scenic. Na Muang is a popular place for families to relax and swim.
                        Many Samui tours include a refreshing stop at the Solo on their itinerary.hough access to the waterfalls is free there is plenty along the way to spend your money on,
                        including several stalls selling snacks and souvenirs and offers of a guided tour of the area. An entire day could be spent at the falls swimming, hiking, exploring, picnicking –
                        a cool and peaceful alternative to the beach.
                    </p>
                </div>
                <div class="slide-footer">
                    <span class="pull-right buttons">
                        <button class="btn btn-sm btn-default"><i class="fa fa-fw fa-eye"></i> Show</button>

                    </span>
                </div>
            </div>
        </div>
        <div class="row carousel-row">
            <div class="col-xs-8 col-xs-offset-2 slide-row">
                <div id="carousel-3" class="carousel slide slide-carousel" data-ride="carousel">
                    <!-- Indicators -->
                    <ol class="carousel-indicators">
                        <li data-target="#carousel-3" data-slide-to="0" class="active"></li>
                        <li data-target="#carousel-3" data-slide-to="1"></li>
                        <li data-target="#carousel-3" data-slide-to="2"></li>
                    </ol>

                    <!-- Wrapper for slides -->
                    <div class="carousel-inner">
                        <div class="item active">
                            <img src="https://image.cermati.com/q_70/zz4c6axrgmvc3speqqgv.webp" alt="Image">
                        </div>
                        <div class="item">
                            <img src="https://image.cermati.com/q_70/mrdlfhgwwbpm3aleevcx.webp" alt="Image">
                        </div>
                        <div class="item">
                            <img src="https://image.cermati.com/q_70/tauggtdvwynrxyaa0ftl.webp" alt="Image">
                        </div>
                    </div>
                </div>
                <div class="slide-content">
                    <h4>Location</h4>
                    <p>
                        Location:Solo Waterfall is off Route 4169 south of Koh Samui’s inland mountains .
                        Remarks: There is no entry charge to the Na Muang falls, so be aware that some local ‘guides’ try to ask for money for access or assistance but this is not necessary since the route is easy to travel on your own.
                        Bring swimming gear and be sure to wear quality sandals or walking shoes to enjoy the walking trails safely.
                        The best time to go is in the wetter months of September through November when the waterfalls are in their full flowing glory.
                        How to get there: From Nathon, drive along the main road for about 11 kilometres until reaching the signed entrance road to Na Muang falls.
                        The park entrance is about one kilometre up this road. Na Muang 1 is reached by a 100-metre walk from the parking lot, while Na Muang 2 is a further 100 metres away along a more challenging path.
                    </p>
                </div>
                <div class="slide-footer">
                    <span class="pull-right buttons">
                        <button class="btn btn-sm btn-default"><i class="fa fa-fw fa-eye"></i> Show</button>

                    </span>
                </div>

            </div>
        </div>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>

</body>

</html>