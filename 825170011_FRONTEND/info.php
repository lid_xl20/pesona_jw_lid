<?php
include "include/config.php";
$ambilberita = mysqli_query($connection, "SELECT * from berita where beritaKODE='B00001' ");
$databerita = mysqli_fetch_array($ambilberita);
?>


<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<link rel="stylesheet" type="text/css" href="css/thebatcs.css">



<nav class="navbar navbar-expand-lg mb-4 top-bar navbar-static-top sps sps--abv">
    <div class="container">
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarCollapse1" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <a class="navbar-brand mx-auto" href="#">Hello Indonesia</a>
        <div class="collapse navbar-collapse" id="navbarCollapse1">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item active"> <a class="nav-link" href="#myCarousel">Home <span class="sr-only">(current)</span></a> </li>
                <li class="nav-item"> <a class="nav-link" href="#contact">Contact</a> </li>
            </ul>
        </div>
    </div>
</nav>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Info Hello Indonesia</title>
</head>

<body>


    <!-- Swiper Silder
    ================================================== -->
    <!-- Slider main container -->
    <div class="swiper-container main-slider" id="myCarousel">
        <div class="swiper-wrapper">
            <div class="swiper-slide slider-bg-position" style="background:url('https://kintamaniid-a903.kxcdn.com/wp-content/uploads/Pantai-yang-lagi-happening-di-Bali-2-1024x681.jpg')" data-hash="slide1">

                <h2>Hello Indonesia Look at the beauty of the Lake Thebat Semarang</h2>
            </div>
            <div class="swiper-slide slider-bg-position" style="background:url('https://cdn.pixabay.com/photo/2017/08/06/00/27/yoga-2587066_960_720.jpg')" data-hash="slide2">
                <h2>Happiness is nothing more than good health and a bad memory</h2>
            </div>
        </div>
        <!-- Add Pagination -->
        <div class="swiper-pagination"></div>
        <!-- Add Navigation -->
        <div class="swiper-button-prev"><i class="fa fa-chevron-left"></i></div>
        <div class="swiper-button-next"><i class="fa fa-chevron-right"></i></div>
    </div>

    <!-- Benefits
    ================================================== -->
    <section class="service-sec" id="benefits">
        <div class="container">
            <div class="row">
                <h2><small> <?php echo $databerita['beritaISI'] ?></small></h2>
                <h2><small> <?php echo $databerita['beritaISI2'] ?></small></h2>
                <div class="col-md-12">
                    <div class="heading">
                        <h2><small>Explore Indonesia Thebat Semarang</small>To enjoy the glow of good health, you must exercise</h2>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-6 text-sm-center service-block"> <i class="fa fa-plus" aria-hidden="true"></i>
                            <h3>Better Lake</h3>
                            <p>One of the most well-known lakes in the Pagaralam community is Tebat Gheban. Located about 7 km from the center of the City of Pagaralam, Tebat</p>
                        </div>
                        <div class="col-md-6 text-sm-center service-block"> <i class="fa fa-leaf" aria-hidden="true"></i>
                            <h3>Pine Trees</h3>
                            <p>Surrounded by pine trees, Tebat Gheban has a beautiful view, in the afternoon the sunlight will glow on the water that flows calmly, while the sound of birds muttering from the trees. Not surprisingly, many people arrive in the afternoon just to enjoy the atmosphere, or to exercise by running around the lake on the trail
                            </p>
                        </div>
                        <div class="col-md-6 text-sm-center service-block"> <i class="fa fa-leaf" aria-hidden="true"></i>
                            <h3>Fort Marlborough</h3>
                            <p>Moat around the fort and a bridge connecting the entrance and the main building, and there is an underground tunnel to Pantai Panjang, Padak Tread and Regional Buildings or often referred to as the Governor’s Palace
                            </p>
                        </div>
                        <div class="col-md-6 text-sm-center service-block"> <i class="fa fa-bell" aria-hidden="true"></i>
                            <h3>Address</h3>
                            <p>Located on Jalan Ahmad Yani, Bengkulu, this fort was a fortress built by the East Indies Company in 1713-1719 and was used as a British stronghold which was under the leadership of the governor Joseph Callet.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="media-bottom">
                    <a href="#">
                        <img class="media-object" style="width:380px;height:500px;margin-top:20px;" ; src="imagesuk/<?php echo $databerita['beritaICONFOTO']; ?>" alt="tidak ada">

                    </a>
                </div>
            </div>
            <!-- /.row -->
        </div>
    </section>

    <!-- About 
    ================================================== -->
    <section class="about-sec parallax-section" id="about">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <h2><small>HELLO</small>About<br>
                        Indonesia</h2>
                    <h2><small> <?php echo $databerita['beritaSUMBER'] ?></small></h2>
                </div>
                <div class="col-md-4">
                    <p>To enjoy good health, to bring true happiness to one's family, to bring peace to all, one must first discipline and control one's own mind. If a man can control his mind he can find the way to Enlightenment.</p>
                    <p>Saving our planet, lifting people out of poverty, advancing economic growth... these are one and the same fight. We must connect the dots between climate change, water scarcity, energy shortages, global health. Solutions to one problem must be solutions for all.</p>
                </div>
                <div class="col-md-4">
                    <p>Our greatest happiness does not depend on the condition of life in which chance has placed us, but is always the result of a good conscience, good health, occupation, and freedom in all just pursuits.</p>
                    <p>Being in control of your life and having realistic expectations about your day-to-day challenges are the keys to stress management</p>

                    <p><a href="#" class="btn btn-transparent-white btn-capsul">Explore More</a></p>
                </div>
            </div>
        </div>
    </section>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
</body>

</html>