<?php
include "include/config.php";
$kecamatan = $_GET['kodekec'];
$ambil = mysqli_query($connection, "SELECT *from obyekwisata where kecamatanKODE='$kecamatan'");
$data = mysqli_fetch_array($ambil);
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Wisata Pesona</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/cssgalerii.css" rel="stylesheet">
</head>
<style>
    .media-body p {
        margin-left: 50px;
    }

    .media-body h1 {
        text-align: center;
    }
</style>
<!--background:paleturquoise;-->

<body>
    <?php
    include("include/menu.php");
    ?>
    <div class="container">

        <img src="images/<?php echo $data['obyekFOTO'] ?>" style="margin-left:30%" width="400px;" height="200px;">
        <div class="jumbotron" style="text-align:center">
            <p>Obyek Wisata</p>
        </div>
        <div class="media-body" style="margin-left:10px;">
            <h1><?php echo $data['obyekNAMA']; ?></h1>
            <!--TAMBAHIN YANG DATA DI OBYEK WISATA-->
            <p>ALAMAT :<?php echo $data['obyekALAMAT']; ?></p>
            <p>Derajat_S :<?php echo $data['obyekDERAJAT_S']; ?></p>
            <p>Menit_S :<?php echo $data['obyekMENIT_S']; ?></p>
            <p>Detik_S :<?php echo $data['obyekDETIK_S']; ?></p>
            <p>Latitude :<?php echo $data['obyekLATITUDE']; ?></p>
            <p>Derajat_E :<?php echo $data['obyekDERAJAT_E']; ?></p>
            <p>Menit_E :<?php echo $data['obyekMENIT_E']; ?></p>
            <p>Detik_E :<?php echo $data['obyekDETIK_E']; ?></p>
            <p>Longitude :<?php echo $data['obyekLONGITUDE']; ?></p>
            <p>Ketinggian :<?php echo $data['obyekKETINGGIAN']; ?></p>
            <p>DEFINISI :<?php echo $data['obyekDEFINISI']; ?></p>
            <p>Keterangan :<?php echo $data['obyekKETERANGAN']; ?></p>
        </div>

        <?php
        include("include/footer.php");
        ?>
    </div>
</body>

</html>