<?php
include "include/config.php";
$kab = $_GET['kodekab'];
$querykab = mysqli_query($connection, "SELECT *FROM kabupaten kab,obyekwisata o,kecamatan kec,kategoriwisata kw where o.kecamatanKODE=kec.kecamatanKODE AND kw.kategoriKODE=o.kategoriKODE AND kab.kabupatenKODE=kec.kabupatenKODE and kab.kabupatenKODE='$kab' ");
$data = mysqli_fetch_array($querykab);
?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Wisata</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/cssgalerii.css" rel="stylesheet">
</head>

<body>
    <?php
    include("include/menu.php");
    ?>
    <div class="container">
        <div class="jumbotron" style="text-align:center">
            <p>Wisata</p>
        </div>
        <div class="row">
            <div class="col-sm-8">
                <div class="media">
                    <div class="media-left">
                        <a href="wisata.php?kodekec=<?php echo $data['kabupatenKODE']; ?>">
                            <img src="imagesuk/<?php echo $data['kabupatenFOTOICON'] ?>" style="margin-top:30%" width="200px;" height="200px;">
                        </a>
                    </div>
                    <div class="media-body">
                        <h1>Obyek Nama:<?php echo $data['obyekNAMA']; ?></h1>
                        <!--TAMBAHIN YANG DATA DI KABUPATEN-->
                        <p>Kabupaten Foto Icon Keterangan:<?php echo $data['kabupatenFOTOICONKET']; ?></p>
                        <br>
                        <p>Obyek Kode :<?php echo $data['obyekKODE']; ?></p>
                        <br>
                        <p>Keterangan kabupaten :<?php echo $data['kabupatenKET']; ?>
                        </p>
                        <div class="jumbotron" style="text-align:center; background:cornflowerblue">
                            <p>Foto Obyek Wisata :</p>
                        </div>
                        <img src="images/<?php echo $data['obyekFOTO'] ?>" style="margin-top:10%" width="500px;" height="200px;">
                        <br>
                        <p>Kategori Kode :<?php echo $data['kategoriKODE']; ?></p>
                        <p>Kecamatan Kode :<?php echo $data['kecamatanKODE']; ?></p>
                        <p>Obyek Alamat :<?php echo $data['obyekALAMAT']; ?></p>
                        <p>Longitude :<?php echo $data['obyekLONGITUDE']; ?></p>
                        <p>Ketinggian :<?php echo $data['obyekKETINGGIAN']; ?></p>
                        <p>DEFINISI :<?php echo $data['obyekDEFINISI']; ?></p>
                        <p>Keterangan :<?php echo $data['obyekKETERANGAN']; ?></p>

                        <br>
                    </div>
                </div>



            </div>
        </div>
        <?php
        include("include/footer.php");
        ?>

    </div>
</body>

</html>