<?php
include('include/config.php');

$detilkabupaten = $_GET['kodekab'];
$query2kabupaten = mysqli_query($connection, "SELECT * FROM kabupaten kab, berita b where kab.kabupatenKODE = b.kabupatenKODE and kab.kabupatenKODE = '$detilkabupaten'"); //kabupaten untuk berita
$query1kabupaten =  mysqli_query($connection, "SELECT * FROM kabupaten kab, kecamatan b where kab.kabupatenKODE = b.kabupatenKODE and kab.kabupatenKODE = '$detilkabupaten'"); //kabupaten untuk kecamatan
?>

<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Kabupaten -Lidya</title>
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/cssgalerii.css" rel="stylesheet">
</head>

<body>
	<?php include('include/menu.php');
	?>
	<div class="container">
		<div class="row">
			<div class="col-sm-8">
				<?php
				if (mysqli_num_rows($query1kabupaten) > 0)
					while ($row1kabupaten = mysqli_fetch_array($query1kabupaten)) { ?>
					<div class="media">
						<div class="media-left">
							<a href="kabupaten.php?kodekab=<?php echo $row1kabupaten["kabupatenKODE"] ?>">
								<img src=" imagesuk/<?php echo $row1kabupaten['kecamatanFOTO'] ?>" style="margin-top:30%" width="200px;" height="200px;">
							</a>
						</div>
						<div class="media-body">
							<h4 class="media-heading"><?php echo $row1kabupaten['kecamatanNAMA']; ?></h4>
							<br>
							<!--info Kabupaten-->
							<h1>Data kabupaten</h1>
							Nama Kabupaten : <?php echo $row1kabupaten['kabupatenNAMA']; ?></h4>
							<br>
							Kabupaten Alamat : <?php echo $row1kabupaten['kabupatenALAMAT']; ?> <br>
							<br>
							Kabupaten Keterangan : <?php echo $row1kabupaten['kabupatenKET']; ?> <br>
							<br>
							<!-- Info kecamatan di kabupaten tersebut-->
							<h1>Data Kecamatan</h1>
							Alamat Kecamatan : <?php echo $row1kabupaten['kecamatanALAMAT']; ?>
							<br>
							Kecamatan Keterangan : <?php echo $row1kabupaten['kecamatanKET']; ?>
							<br>
							Kecamatan Tanggal : <?php echo $row1kabupaten['kecamatanTGL']; ?>
							<br>
							<br>

						</div>
					</div>
				<?php } ?>
			</div>
		</div>
	</div>

	<?php include('include/footer.php'); ?>
</body>

</html>