<?php
include("include/config.php");

ob_start();
session_start(); /*sessionnya sudah mulai dibuka*/
if (!isset($_SESSION['namauser'])) /*sudah diisi panggil file index ,,pakai ! jika session tidak aktif maka dipanggil latlogin*/
  header("location:login.php");
?>



<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>Pesona Indonesia 'Lidya'</title>

  <!-- Bootstrap -->
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <link href="css/cssgalerii.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>



  <!-- Tempat kerjaku -->

  <!-- Membuat menu -->

  <?php
  include("include/menu.php");

  ?>

  <!-- AKhir menu -->
  <!--slider-->
  <?php
  include("include/slid.php");

  ?>

  <?php
  include("include/jumbotron.php");

  ?>

  <!--akhir kolom berita-->
  <?php
  include("include/berita.php");
  ?>




  <!--akhir tempat kerjaku-->

  <!--galeri foto-->

  <?php
  include("include/galerifoto.php");

  ?>



  <?php
  include("include/footer.php");

  ?>





</body>

</html>

<?php
mysqli_close($connection);
/** tutup ob start **/
ob_end_flush();
?>