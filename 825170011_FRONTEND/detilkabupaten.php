<?php

include "include/config.php";

?>
<?php
$kabupaten = $_GET['detilkabupaten'];


$edit = mysqli_query($connection, "SELECT *FROM kabupaten where kabupatenKODE='$kabupaten'");

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Detail Kabupaten</title>
    <link href="css/style.css" rel="stylesheet" type="text/css">
</head>

<body>
    <?php
    include "include/slider.php"; ?>
    <table class="table table-hover">
        <caption>
            <h1>Informasi Kabupaten </h1>
        </caption>
        <tr class="success">
            <th> No </th>
            <th> Kode Kabupaten </th>
            <th> Nama Kabupaten </th>
            <th> Alamat Kabupaten </th>
            <th> Keterangan Kabupaten </th>
            <th> Foto Icon</th>
            <th>Foto Keterangan</th>

        </tr>

        <?php

        if (mysqli_num_rows($edit) > 0)
            ?>
        <?php $nomor = 1;
    ?>
        <?php
        while ($row = mysqli_fetch_array($edit)) {
            ?>
            <!--sesuai nama tabel yang dibawah ini -->
            <tr class="info">
                <td> <?php echo $nomor ?></td>
                <td><?php echo $row['kabupatenKODE']; ?></td>
                <td><?php echo $row['kabupatenNAMA']; ?></td>
                <td> <?php echo $row['kabupatenALAMAT']; ?></td>
                <td><?php echo $row['kabupatenKET']; ?></td>
                <td>
                    <img src="images/<?php echo $row['kabupatenFOTOICON'] ?>" width="80">
                </td>
                <td><?php echo $row['kabupatenFOTOICONKET']; ?></td>
            </tr>

            <?php $nomor++; ?>
        <?php
        }  ?>

    </table>
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">

</body>

</html>