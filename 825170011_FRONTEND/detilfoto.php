<?php
include "include/config.php";
$foto = $_GET['detilfoto'];
$ambil = mysqli_query($connection, "SELECT *from fotoobyekwisata where obyekKODE='$foto'");
$data = mysqli_fetch_array($ambil);
?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Foto Wisata</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/cssgalerii.css" rel="stylesheet">
</head>

<body>
    <?php
    include("include/menu.php");
    ?>
    <div class="container">
        <div class="jumbotron" style="text-align:center">
            <p>Keterangan Foto Wisata</p>
        </div>
        <div class="row">
            <div class="col-sm-8">
                <div class="media">
                    <div class="media-left">
                        <img src=" images/<?php echo $data['fotoobyekGAMBAR'] ?>" style="margin-top:30%" width="200px;" height="200px;" margin-bottom="30%;">
                        </a>
                    </div>
                    <div class="media-body" style="margin-left:10px;">
                        <h1>Nama Foto Obyek :<?php echo $data['fotoobyekNAMA']; ?></h1>
                        <p>Foto Obyek Kode :<?php echo $data['fotoobyekKODE']; ?></p>
                        <p>Obyek Kode :<?php echo $data['obyekKODE']; ?></p>
                        <p>Obyek Alamat :<?php echo $data['fotoobyekKET']; ?></p>
                        <p>Foto Obyek Tanggal :<?php echo $data['fotoobyekTGLAMBIL']; ?></p>
                    </div>
                </div>
            </div>
        </div>
        <?php
        include("include/footer.php");
        ?>


    </div>

</body>

</html>