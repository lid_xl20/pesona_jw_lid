<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Siangau beach</title>
</head>

<body>
    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
            <li data-target="#carousel-example-generic" data-slide-to="2"></li>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
            <div class="item active">
                <!--ubah-->
                <img src="imagesuk/siangau2.jpg" alt="Gambar ga ada">
                <div class="carousel-caption">
                    <h1> Foto Siangau Beach</h1>
                    <h3> kawasan wisata di Bangka </h3>

                </div>
            </div>
            <div class="item">
                <img src="imagesuk/siangau1.jpg" alt="Gambar ga ada">
                <div class="carousel-caption">
                    <h1> Foto Pesona Destinasi Explore Babel</h1>
                    <h3> kawasan terkenal di Tambon Kamala</h3>
                </div>
            </div>

            <div class="item">
                <img src="imagesuk/siangau3.jpg" alt="Gambar ga ada">
                <div class="carousel-caption">
                    <h1> Foto kawasan Teluk Limau </h1>
                    <h3> @Rcinema.Art </h3>
                </div>
            </div>
        </div>



        <!-- Controls -->
        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <link rel="stylesheet" type="text/css" href="css/thailand.css">
    <!------ Include the above in your HEAD tag ---------->

    <div class="container">
        <div class="row">
            <div class="col-xs-8 col-xs-offset-2">
                <h4>Made by <a href="https://facebook.com/lidya.wang.9" class="btn btn-primary btn-xs" target="_blank"><i class="fa fa-fw fa-facebook"></i>@lidyawang</a></h4>
            </div>
        </div>
        <hr>
        <!-- Begin of rows -->
        <div class="row carousel-row">
            <div class="col-xs-8 col-xs-offset-2 slide-row">
                <div id="carousel-1" class="carousel slide slide-carousel" data-ride="carousel">
                    <!-- Indicators -->
                    <ol class="carousel-indicators">
                        <li data-target="#carousel-1" data-slide-to="0" class="active"></li>
                        <li data-target="#carousel-1" data-slide-to="1"></li>
                        <li data-target="#carousel-1" data-slide-to="2"></li>
                    </ol>

                    <!-- Wrapper for slides -->
                    <div class="carousel-inner">
                        <div class="item active">
                            <img src="https://2.bp.blogspot.com/-T_TBlkGIfDY/VhUf7u-JcjI/AAAAAAAAAMs/RU6fJRctfvc/s640/P1013797%2Bcopy.jpg" alt="Image">
                        </div>
                        <div class="item">
                            <img src="https://2.bp.blogspot.com/-eIYmtEte5w4/VhUf4YeDZjI/AAAAAAAAAMk/OQSatK5nT6A/s640/P1013772%2Bcopy.jpg" alt="Image">
                        </div>
                        <div class="item">
                            <img src="https://4.bp.blogspot.com/-OUzbUkVwgyk/VhUgJAIrF8I/AAAAAAAAAM0/S3Fr4U-Aq0c/s640/P1013794%2Bcopy.jpg" alt="Image">
                        </div>
                    </div>
                </div>
                <div class="slide-content">
                    <h4>Objek Wisata Pantai Teluk Limau</h4>
                    <p>
                        Objek wisata pantai Teluk Limau adalah salah satu kawasan wisata di pantai matras.
                        Pantai teluk limau bangka ini memiliki lebih banyak batu dari pada pasir pantai . Menurut nelayan setempat ,
                        pantai ini memiliki terumbu karang yang cukup bagus .
                        Namun hingga saat ini belum ada keterangan yang pasti untuk titik - titik
                        dimana posisi terumbu karang yang bagus tersebut . Karena hingga saat ini Pantai teluk limau bangka masih belum adanya
                        "sentuhan" yang serius dari pemerintah setempat.
                    </p>
                </div>
                <div class="slide-footer">
                    <span class="pull-right buttons">
                        <button class="btn btn-sm btn-default"><i class="fa fa-fw fa-eye"></i> Show</button>

                    </span>
                </div>
            </div>
        </div>
        <div class="row carousel-row">
            <div class="col-xs-8 col-xs-offset-2 slide-row">
                <div id="carousel-2" class="carousel slide slide-carousel" data-ride="carousel">
                    <!-- Indicators -->
                    <ol class="carousel-indicators">
                        <li data-target="#carousel-2" data-slide-to="0" class="active"></li>
                        <li data-target="#carousel-2" data-slide-to="1"></li>
                        <li data-target="#carousel-2" data-slide-to="2"></li>
                    </ol>

                    <!-- Wrapper for slides -->
                    <div class="carousel-inner">
                        <div class="item active">
                            <img src="https://2.bp.blogspot.com/-jyBVQOV7sO4/VhUgVeskD1I/AAAAAAAAAM8/BIOUDOnvy4U/s640/P1013766%2Bcopy.jpg" alt="Image">
                        </div>
                        <div class="item">
                            <img src="https://4.bp.blogspot.com/-JDFsmByd3rc/VhUgXc91w_I/AAAAAAAAANE/xiAb3Nmpgjk/s640/P1013773%2Bcopy.jpg" alt="Image">
                        </div>
                        <div class="item">
                            <img src="https://2.bp.blogspot.com/-Am1kypcbLfI/VhUg2LwTJ4I/AAAAAAAAANM/5_pLUqHfIds/s640/P1013764%2Bcopy.jpg" alt="Image">
                        </div>
                    </div>
                </div>
                <div class="slide-content">
                    <h4>Interest in tour?</h4>
                    <p>

                        Di teluk limau ini memiliki kawasan pasir pantai ( bibir pantai ) yang cukup sempit, pantai ini dominan di hiasi oleh bebatuan granit.
                        Bahkan dari sebagian batu granit ada batu granit yang paling unik yang besarnya sebesar rumah yang berada tepat di hkawasan pantai teluk limau ini.
                        Di dalam bebatuan granit yang besar ini, ada celah yang sangat unik yang bisa di manfaatkan untuk berfoto.
                    </p>
                </div>
                <div class="slide-footer">
                    <span class="pull-right buttons">
                        <button class="btn btn-sm btn-default"><i class="fa fa-fw fa-eye"></i> Show</button>

                    </span>
                </div>
            </div>
        </div>
        <div class="row carousel-row">
            <div class="col-xs-8 col-xs-offset-2 slide-row">
                <div id="carousel-3" class="carousel slide slide-carousel" data-ride="carousel">
                    <!-- Indicators -->
                    <ol class="carousel-indicators">
                        <li data-target="#carousel-3" data-slide-to="0" class="active"></li>
                        <li data-target="#carousel-3" data-slide-to="1"></li>
                        <li data-target="#carousel-3" data-slide-to="2"></li>
                    </ol>

                    <!-- Wrapper for slides -->
                    <div class="carousel-inner">
                        <div class="item active">
                            <img src="https://2.bp.blogspot.com/-Tqo-PfGrMkI/UqtHUk4YwZI/AAAAAAAAAH8/ZIFCEE06rxg/s1600/429751_2675396416407_1673774686_n.jpg" alt="Image">
                        </div>
                        <div class="item">
                            <img src="https://2.bp.blogspot.com/-_kP5j58xy1w/UqtHlenKvDI/AAAAAAAAAIE/Y2uwXe2CiIE/s1600/431134_2710990306232_665004541_n.jpg" alt="Image">
                        </div>
                        <div class="item">
                            <img src="https://1.bp.blogspot.com/-mipKxMrNv2g/UrWNW9XPjTI/AAAAAAAAI4o/Im5loghOxKE/s400/PA149901.jpg" alt="Image">
                        </div>
                    </div>
                </div>
                <div class="slide-content">
                    <h4>Location</h4>
                    <p>
                        Tepatnya di daerah kampung Matras, Desa Sinar Baru, Kecamatan Sungai Liat,
                        Kabupaten Bangka, Provinsi Bangka Belitung.
                        Untuk menuju pantai ini di butuhkan waktu sekitar 45 menit dari bandara atau 15 menit dari kota sungailiat.
                        Dikarenakan pantai ini berada satu gerbang dengan gerbang pantai matras, maka pantai ini berada di kawasan wisata pantai matras.
                    </p>
                </div>
                <div class="slide-footer">
                    <span class="pull-right buttons">
                        <button class="btn btn-sm btn-default"><i class="fa fa-fw fa-eye"></i> Show</button>

                    </span>
                </div>

            </div>
        </div>
    </div>





    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>

</body>

</html>