<?php include "header.php"; ?>
<?php
include "config.php";
if (isset($_POST['Simpan'])) {
  if (isset($_REQUEST["inputprovkode"])) {
    $provinsiKODE = $_REQUEST['inputprovkode'];
  }
  if (!empty($provinsiKODE)) {
    $provinsiKODE = $_POST['inputprovkode'];
  }

  $provinsiKODE = $_GET['kodeprovinsi'];
  $provinsiNAMA = $_POST['inputprovnama'];
  $kabupatenKODE = $_POST['inputkabupatenkode'];
  $kecamatanKODE = $_POST['inputkecamatankode'];
  $ibukotaPROVINSI = $_POST['inputibukotaprovinsi'];
  $luasWILAYAH = $_POST['inputluaswilayah'];

  /** untuk foto */
  $nama = $_FILES['file']['name'];
  $file_tmp = $_FILES['file']['tmp_name'];
  move_uploaded_file($file_tmp, 'images/' . $nama);

  //untuk menambahkan variabel $gambarlama untuk tidak ingin mengupdate foto lain 
  $gambarlama = $_POST["gambarlama"];
  // cek gambar lama atau baru 
  if ($_FILES['file']['error'] === 4) {
    $nama = $gambarlama;
  }


  mysqli_query($connection, "UPDATE provinsi SET provinsiNAMA='$provinsiNAMA',kabupatenKODE='$kabupatenKODE',kecamatanKODE='$kecamatanKODE',ibukotaPROV='$ibukotaPROVINSI',luasWILAYAH='$luasWILAYAH',provinsiFOTO='$nama' where provinsiKODE='$provinsiKODE'");
  header("location:provinsi.php");
  /** untuk menampilkan update berhasil tidak **/
  if (($_POST) > 0) {
    echo "<script>
       alert('data berhasil diupdate!');
       document.location.href= 'provinsi.php'
       </script>";
  } else {
    echo "<script>
       alert('data gagal diupdate!');
       document.location.href= 'provinsi.php'
       </script>";
  }
}
$provinsikode = $_GET['kodeprovinsi'];
$edit = mysqli_query($connection, "SELECT *FROM provinsi where provinsiKODE='$provinsikode'");
$row_edit = mysqli_fetch_array($edit);
$query = mysqli_query($connection, "SELECT *FROM provinsi");
$squery = mysqli_query($connection, "SELECT *FROM kabupaten");
$tquery = mysqli_query($connection, "SELECT *FROM kecamatan");

?>

<section id="main-content">
  <section class="wrapper">
    <div class="row">
      <div class="col-lg-12">
        <h3 class="page-header"><i class="fa fa-laptop"></i> Provinsi</h3>
        <ol class="breadcrumb">
          <li><i class="fa fa-home"></i><a href="index.php">Home</a></li>
          <li><i class="fa fa-laptop"></i>Provinsi</li>
        </ol>
      </div>
    </div>
  </section>

  <div class="col-sm-10">
    <form method="POST" action="" class="form-horizontal" enctype="multipart/form-data">
      <div class="form-group form-group-lg">
        <label class="col-sm-3 control-label" for="inputprovkode">Provinsi Kode </label>
        <div class="col-sm-6">
          <input class="form-control" type="text" id="inputprovkode" name="inputprovkode" placeholder="Kode Prov" value="<?php echo $row_edit["provinsiKODE"] ?>" disabled>
        </div>
      </div>

      <div class="form-group form-group-lg">
        <label class="col-sm-3 control-label" for="inputprovnama">Nama Provinsi</label>
        <div class="col-sm-6">
          <input class="form-control" type="text" id="inputprovnama" name="inputprovnama" placeholder="NamaProv" value="<?php echo $row_edit["provinsiNAMA"] ?>">
        </div>
      </div>

      <div class="form-group form-group-lg">
        <label class="col-sm-3 control-label" for="inputkabupatenkode">Kode Kabupaten</label>
        <div class="col-sm-6">
          <select name="inputkabupatenkode" class="form-control">
            <option value="kabupaten">Kabupaten Kode</option>
            <?php if (mysqli_num_rows($squery) > 0) { ?>
              <?php while ($row = mysqli_fetch_array($squery)) { ?>
                <option>
                  <?php echo $row["kabupatenKODE"] ?>
                  <?php echo $row["kabupatenNAMA"] ?>
                </option>
              <?php } ?>
            <?php } ?>
            </option>
          </select>
        </div>
      </div>

      <div class="form-group form-group-lg">
        <label class="col-sm-3 control-label" for="inputkecamatankode">Kode Kecamatan Kategori</label>
        <div class="col-sm-6">
          <select name="inputkecamatankode" class="form-control">
            <option value="kecamatan">Kategori Kecamatan Kode</option>
            <?php if (mysqli_num_rows($tquery) > 0) { ?>
              <?php while ($row = mysqli_fetch_array($tquery)) { ?>
                <option>
                  <?php echo $row["kecamatanKODE"] ?>
                  <?php echo $row["kecamatanNAMA"] ?>
                </option>
              <?php } ?>
            <?php } ?>
            </option>
          </select>
        </div>
      </div>

      <div class="form-group form-group-lg">
        <label class="col-sm-3 control-label" for="inputibukotaprovinsi">Nama Ibukota</label>
        <div class="col-sm-6">
          <input class="form-control" type="text" id="inputibukotaprovinsi" name="inputibukotaprovinsi" placeholder="Nama Ibukota" value="<?php echo $row_edit["ibukotaPROV"] ?>">
        </div>
      </div>

      <div class="form-group form-group-lg">
        <label class="col-sm-3 control-label" for="inputluaswilayah">Luas Wilayah</label>
        <div class="col-sm-6">
          <input class="form-control" type="text" id="inputluaswilayah" name="inputluaswilayah" placeholder="Nama Luas" value="<?php echo $row_edit["luasWILAYAH"] ?>">
        </div>
      </div>

      <!-- untuk bagian input diberi hidden untuk gambarlama supaya pas edit tetap ada-->
      <input type="hidden" name="gambarlama" value="<?= $row_edit["provinsiFOTO"]; ?>">

      <div class="form-group form-group-lg">
        <label class="col-sm-3 control-label" for="file">FotoProvinsi</label>
        <img src="images/<?php echo $row_edit['provinsiFOTO'] ?>" width="80">
        <div class="col-sm-6">
          <input type="file" id="file" name="file">
          <p class="help-block">Untuk Unggah file Provinsi</p>
        </div>
      </div>

      <div class="col-sm-3">
      </div>
      <div class="col-sm-3">
        <input class="btn btn-lg btn-primary" type="submit" value="Simpan" name="Simpan">
        <!-- tombol diperbesar dg -lg dan berwarna biru dengan -primary -->
        <input class="btn btn-lg btn-info" type="reset" value="Batal">
        <!-- tombol berwarna hijau langit -->
      </div>
    </form>
  </div>
</section>

<?php include "footer.php"; ?>