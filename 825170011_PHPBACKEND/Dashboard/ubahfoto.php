<?php
    include "config.php";
    $query = mysqli_query($connection, "SELECT * FROM editfoto");    
?>
 
<html>
<head>
    <title>KATEGORI WISATA</title>
</head>
<body>
    <h2>MEMPERBAHARUI FOTO</h2>
    <table border="1">
      <thead>
        <tr>
          <th>No</th>
          <th>Kode Foto</th>
          <th>Nama Foto</th>
          <th>Kategori Kode</th>
          <th>Gambar Foto</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
      <?php if(mysqli_num_rows($query)>0)
      {?>
        <?php $nomor=1;
        while ($row = mysqli_fetch_array($query))
        {?>
        <tr>
          <th scope="row"><?php echo $nomor; ?></th>
          <td><?php echo $row["fotoKODE"];?></td>
          <td><?php echo $row["fotoNAMA"];?></td>
          <td><?php echo $row["kategoriKODE"];?></td>
          <td>
            <?php if($row['fotoNAMA']==""){ echo "<img src='iconfoto/noimage.png' width='88'/>";}else{?>
            <img src="images/<?php echo $row['fotoNAMA'] ?>" width="88" class="img-responsive" />
            <?php }?>
          </td>      
          <td>
                <a href="updatefoto.php?kodefoto=<?php echo $row["fotoKODE"]?>">EDIT</a>
                <a href="hapusfoto2.php?kodefoto=<?php echo $row["fotoKODE"]?>">DELETE</a>           
          </td>      
          </tr>
        <?php $nomor++; }?>
      <?php } ?>   
      </tbody>
    </table>   
</body>
</html>