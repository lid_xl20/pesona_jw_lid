<?php include "header.php"; ?>
<?php
include "config.php";
if (isset($_POST['Simpan'])) {
	if (isset($_REQUEST["inputobyekkode"])) {
		$obyekKODE = $_REQUEST["inputobyekkode"];
	}

	if (!empty($obyekKODE)) {
		$obyekKODE = $_POST['inputobyekkode'];
	}
	//tanda petik 1 dan 2 sama
	$obyekKODE = $_POST['inputobyekkode'];
	$obyekNAMA = $_POST['inputobyeknama'];
	$kecamatanKODE = $_POST['inputkecamatankode'];
	$kategoriKODE = $_POST['kategoriKODE'];
	$obyekALAMAT = $_POST['inputobyekalamat'];
	$obyekDERAJAT = $_POST['inputobyekderajat'];
	$obyekMENIT = $_POST['inputobyekmenit'];
	$obyekDETIK = $_POST['inputobyekdetik'];
	$obyekLATITUDE = $_POST['inputobyeklatitude'];
	$obyekDERAJAT_E = $_POST['inputobyekderajat_e'];
	$obyekMENIT_E = $_POST['inputobyekmenit_e'];
	$obyekDETIK_E = $_POST['inputobyekdetik_e'];
	$obyekLONGITUDE = $_POST['inputobyeklongitude'];
	$obyekKETINGGIAN = $_POST['inputobyekketinggian'];
	$obyekDEFINISI = $_POST['inputobyekdefinisi'];
	$obyekKETERANGAN = $_POST['inputobyekketerangan'];



	/** untuk foto */
	$nama = $_FILES['file']['name'];
	$file_tmp = $_FILES['file']['tmp_name'];
	move_uploaded_file($file_tmp, 'images/' . $nama);

	//connection harus sama dengan file di config.php
	mysqli_query($connection, "INSERT INTO obyekwisata VALUES('$obyekKODE','$obyekNAMA','$kecamatanKODE','$kategoriKODE','$obyekALAMAT','$obyekDERAJAT','$obyekMENIT'
    ,'$obyekDETIK','$obyekLATITUDE','$obyekDERAJAT_E','$obyekMENIT_E','$obyekDETIK_E','$obyekLONGITUDE','$obyekKETINGGIAN','$obyekDEFINISI','$obyekKETERANGAN','$nama')");
	header("location:destinasiwisata.php");
}


$query = mysqli_query($connection, "SELECT *FROM obyekwisata");
$squery = mysqli_query($connection, "SELECT *FROM kecamatan");
$tquery = mysqli_query($connection, "SELECT *FROM kategoriwisata");

/** variabel utk dibagi dan ditampilkan per berapa record **/
$jumlahtampil = 3;
$halaman = @$_GET['page'];
$nomorpage = 1;
if (empty($halaman)) {
	$posisi = 0;
	$halaman = 1;
} else {
	$posisi = ($halaman - 1) * $jumlahtampil;
}
$query = mysqli_query($connection, "SELECT *FROM obyekwisata limit $posisi,$jumlahtampil");
?>


<section id="main-content">
	<section class="wrapper">
		<div class="row">
			<div class="col-lg-12">
				<h3 class="page-header"><i class="fa fa-laptop"></i> Destinasi Wisata</h3>
				<ol class="breadcrumb">
					<li><i class="fa fa-home"></i><a href="index.php">Home</a></li>
					<li><i class="fa fa-laptop"></i>Destinasi WIsata</li>
				</ol>
			</div>
		</div>
	</section>


	<div class="col-sm-10">
		<form method="POST" action="" class="form-horizontal" enctype="multipart/form-data">
			<div class="form-group form-group-lg">
				<label class="col-sm-3 control-label" for="obyekkode">Obyek Kode </label>
				<div class="col-sm-6">
					<input class="form-control" type="text" id="obyekkode" name="inputobyekkode" placeholder="Kode Obyek" maxlength="4" required="">
				</div>
			</div>

			<div class="form-group form-group-lg">
				<label class="col-sm-3 control-label" for="obyeknama">Nama Obyek Wisata</label>
				<div class="col-sm-6">
					<input class="form-control" type="text" id="obyeknama" name="inputobyeknama" placeholder="Nama Obyek">
				</div>
			</div>

			<div class="form-group form-group-lg">
				<label class="col-sm-3 control-label" for="KecamatanKODE">Kode Kecamatan Kategori</label>
				<div class="col-sm-6">
					<select name="inputkecamatankode" class="form-control">
						<option value="kecamatan">Kategori Kecamatan Kode</option>
						<?php if (mysqli_num_rows($squery) > 0) { ?>
							<?php while ($row = mysqli_fetch_array($squery)) { ?>
								<option>
									<?php echo $row["kecamatanKODE"] ?>
								</option>
							<?php } ?>
						<?php } ?>
						</option>
					</select>
				</div>
			</div>

			<div class="form-group form-group-lg">
				<label class="col-sm-3 control-label" for="Kabupaten">Kode Kategori</label>
				<div class="col-sm-6">
					<select name="kategoriKODE" class="form-control">
						<option value="kategori">Kategori Kode</option>

						<?php if (mysqli_num_rows($tquery) > 0) { ?>
							<?php while ($row = mysqli_fetch_array($tquery)) { ?>

								<option>

									<?php echo $row["kategoriKODE"] ?>
								</option>

							<?php } ?>
						<?php } ?>
						</option>

					</select>

				</div>
			</div>
			<div class="form-group form-group-lg">
				<label class="col-sm-3 control-label" for="obyekalamat">Obyek Alamat</label>
				<div class="col-sm-6">
					<input class="form-control" type="text" id="obyekalamat" name="inputobyekalamat" placeholder="Obyek Alamat">
				</div>
			</div>

			<div class="form-group form-group-lg">
				<label class="col-sm-3 control-label" for="obyekderajat">Obyek Derajat</label>
				<div class="col-sm-6">
					<input class="form-control" type="text" id="obyekderajat" name="inputobyekderajat" placeholder="Obyek Derajat">
				</div>
			</div>

			<div class="form-group form-group-lg">
				<label class="col-sm-3 control-label" for="obyekmenit">Obyek Menit</label>
				<div class="col-sm-6">
					<input class="form-control" type="text" id="obyekmenit" name="inputobyekmenit" placeholder="Obyek Menit">
				</div>
			</div>
			<div class="form-group form-group-lg">
				<label class="col-sm-3 control-label" for="obyekdetik">Obyek Detik</label>
				<div class="col-sm-6">
					<input class="form-control" type="text" id="obyekdetik" name="inputobyekdetik" placeholder="Obyek Detik">
				</div>
			</div>
			<div class="form-group form-group-lg">
				<label class="col-sm-3 control-label" for="obyeklatitude">Obyek Latitude</label>
				<div class="col-sm-6">
					<input class="form-control" type="text" id="inputobyeklatitude" name="inputobyeklatitude" placeholder="Obyek Longitude">
				</div>
			</div>
			<div class="form-group form-group-lg">
				<label class="col-sm-3 control-label" for="obyekderajat_e">Obyek Derajat_E</label>
				<div class="col-sm-6">
					<input class="form-control" type="text" id="obyekderajat_3" name="inputobyekderajat_e" placeholder="Obyek Derajat_e">
				</div>
			</div>
			<div class="form-group form-group-lg">
				<label class="col-sm-3 control-label" for="obyekmenit_e">Obyek menit_E</label>
				<div class="col-sm-6">
					<input class="form-control" type="text" id="obyekmenit_3" name="inputobyekmenit_e" placeholder="Obyek menit_e">
				</div>
			</div>

			<div class="form-group form-group-lg">
				<label class="col-sm-3 control-label" for="obyekdetik_e">Obyek Detik_E</label>
				<div class="col-sm-6">
					<input class="form-control" type="text" id="obyekdetik_3" name="inputobyekdetik_e" placeholder="Obyek Detik_e">
				</div>
			</div>
			<div class="form-group form-group-lg">
				<label class="col-sm-3 control-label" for="obyeklongitude">Obyek Longitude</label>
				<div class="col-sm-6">
					<input class="form-control" type="text" id="obyeklongitude" name="inputobyeklongitude" placeholder="Obyek longitude">
				</div>
			</div>

			<div class="form-group form-group-lg">
				<label class="col-sm-3 control-label" for="obyekketinggian">Obyek Ketinggian</label>
				<div class="col-sm-6">
					<input class="form-control" type="text" id="obyekketinggian" name="inputobyekketinggian" placeholder="Obyek Ketinggian">
				</div>
			</div>
			<div class="form-group form-group-lg">
				<label class="col-sm-3 control-label" for="obyekdefinisi">Obyek Definisi</label>
				<div class="col-sm-6">
					<input class="form-control" type="text" id="obyekdefinisi" name="inputobyekdefinisi" placeholder="Obyek Definisi">
				</div>
			</div>
			<div class="form-group form-group-lg">
				<label class="col-sm-3 control-label" for="obyekdefinisi">Obyek Keterangan</label>
				<div class="col-sm-6">
					<input class="form-control" type="text" id="obyekdefinisi" name="inputobyekketerangan" placeholder="Obyek Definisi">
				</div>
			</div>
			<div class="form-group form-group-lg">
				<label class="col-sm-3 control-label" for="file">Foto Obyek</label>
				<div class="col-sm-6">
					<input type="file" id="file" name="file">
					<p class="help-block">Untuk Unggah file Obyek</p>
				</div>
			</div>

			<div class="col-sm-3">
			</div>
			<div class="col-sm-3">
				<input class="btn btn-lg btn-primary" type="submit" value="Simpan" name="Simpan">
				<!-- tombol diperbesar dg -lg dan berwarna biru dengan -primary -->
				<input class="btn btn-lg btn-info" type="reset" value="Batal"> <!-- tombol berwarna hijau langit -->
			</div>
		</form>

		<table class="table table-hover">
			<!-- membuat judul -->
			<tr class="info">
				<th>NO</th>
				<th>Kode Obyek Wisata</th>
				<th>Obyek Nama</th>
				<th>Kecamatan Kode</th>
				<th>Kategori Kode</th>
				<th>Obyek Alamat</th>
				<th>Obyek Derajat</th>
				<th>Obyek Menit</th>
				<th>Obyek Detik</th>
				<th>Obyek Latitude</th>
				<th>Obyek Derajat_e</th>
				<th>Obyek menit_e</th>
				<th>Obyek Detik_e</th>
				<th>Obyek Longitude</th>
				<th>Obyek Ketinggian</th>
				<th> Obyek Definisi</th>
				<th> Obyek Keterangan</th>
				<th> Foto Obyek</th>
				<th>Action</th>
			</tr>


			<?php
			/** Memeriksa apakah data yang dipanggil tersebut tersedia atau tidak **/
			if (mysqli_num_rows($query) > 0) { ?>
				<?php $no = 1;
					$no = 1 + $posisi; ?>
				<?php while ($row = mysqli_fetch_array($query)) { ?>
					<tr class="danger">
						<!--sama dengan nama tabel-->
						<td><?php echo $no; ?></td>
						<td><?php echo $row['obyekKODE']; ?> </td>
						<td><?php echo $row['obyekNAMA']; ?> </td>
						<td><?php echo $row['kecamatanKODE']; ?> </td>
						<td><?php echo $row['kategoriKODE']; ?> </td>
						<td><?php echo $row['obyekALAMAT']; ?> </td>
						<td><?php echo $row['obyekDERAJAT_S']; ?> </td>
						<td><?php echo $row['obyekMENIT_S']; ?> </td>
						<td><?php echo $row['obyekDETIK_S']; ?> </td>
						<td><?php echo $row['obyekLATITUDE']; ?> </td>
						<td><?php echo $row['obyekDERAJAT_E']; ?> </td>
						<td><?php echo $row['obyekMENIT_E']; ?> </td>
						<td><?php echo $row['obyekDETIK_E']; ?> </td>
						<td><?php echo $row['obyekLONGITUDE']; ?> </td>
						<td><?php echo $row['obyekKETINGGIAN']; ?> </td>
						<td><?php echo $row['obyekDEFINISI']; ?> </td>
						<td><?php echo $row['obyekKETERANGAN']; ?> </td>
						<td>
							<img src="images/<?php echo $row['obyekFOTO'] ?>" width="80">

						</td>
						<td>
							<a href="destinasiupdate.php?kodeobyek=<?php echo $row["obyekKODE"] ?>">EDIT</a>
							<a href="destinasidelete.php?kodeobyek=<?php echo $row["obyekKODE"] ?>">DELETE</a>
					</tr>
					<?php $no++; ?>
				<?php  } ?>
			<?php  } ?>
		</table>

		<?php
		$hasilrecord = mysqli_query($connection, "SELECT *from obyekwisata");
		$jumlahrecord = mysqli_num_rows($hasilrecord);
		$jumlahpage = ceil($jumlahrecord / $jumlahtampil);

		?>
		<!-- PAGINATION untuk bagian pagination   https://getbootstrap.com/docs/4.3/components/pagination/-->
		<nav aria-label="Page navigation example">
			<ul class="pagination">
				<li class="page-item"><a class="page-link" href="?halaman=<?php echo $nomorpage ?>">Pertama</a></li>
				<li class="page-item">
					<?php for ($nomorpage = 1; $nomorpage <= $jumlahpage; $nomorpage++) {
						if ($nomorpage != $halaman) {
							?><a href="?page=<?php echo $nomorpage ?>"><?php echo $nomorpage; ?></a>
						<?php } else {
								?><a href="?page=<?php echo $nomorpage ?>"><?php echo $nomorpage; ?></a><?php
																											}
																										}
																										?>
				</li>
				<li class="page-item"><a class="page-link" href="?page=<?php echo $nomorpage - 1 ?>">Terakhir</a></li>
			</ul>
		</nav>
	</div>
</section>






<?php include "footer.php"; ?>