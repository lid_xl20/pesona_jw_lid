<?php include "header.php"; ?>
<?php
include "config.php";
/** Mengecek apakah tombol simpan sudah di pilih/klik atau belum **/
if (isset($_POST['Simpan'])) {
	if (isset($_REQUEST["inputKecamatanKode"])) {
		$kecamatanKODE = $_REQUEST["inputKecamatanKode"];
	}
	if (!empty($kecamatanKODE)) {
		$kecamatanKODE = $_POST['inputKecamatanKode'];
	}
	$kecamatanKODE = $_GET['kodekec'];
	$kecamatanNAMA = $_POST['inputKecamatanNama'];
	$kecamatanALAMAT = $_POST['inputKecamatanAlamat'];
	$kecamatanKET = $_POST['inputKecamatanKet'];
	$kabupatenKODE = $_POST['kabupatenKODE'];
	$kecamatanTANGGAL = $_POST['kecamatanTGL'];

	/**untuk nama file gambar */
	$nama = $_FILES['file']['name'];
	$file_tmp = $_FILES['file']['tmp_name'];
	move_uploaded_file($file_tmp, 'images/' . $nama);

	//untuk menambahkan variabel $gambarlama untuk tidak ingin mengupdate foto lain 
	$gambarlama = $_POST["gambarlama"];
	// cek gambar lama atau baru 
	if ($_FILES['file']['error'] === 4) {
		$nama = $gambarlama;
	}
	/** Memasukkan data fullname ke dalam tabel kecamatan **/
	mysqli_query($connection, "UPDATE kecamatan set kecamatanNAMA='$kecamatanNAMA',kecamatanALAMAT='$kecamatanALAMAT',kecamatanKET='$kecamatanKET',
     kecamatanTGL='$kecamatanTANGGAL',kecamatanFOTO='$nama',kabupatenKODE='$kabupatenKODE' where kecamatanKODE='$kecamatanKODE'");

	header("location:kecamatan.php");
	/** untuk menampilkan update berhasil tidak **/
	if (($_POST) > 0) {
		echo "<script>
       alert('data berhasil diupdate!');
       document.location.href= 'kecamatan.php'
       </script>";
	} else {
		echo "<script>
       alert('data gagal diupdate!');
       document.location.href= 'kecamatan.php'
       </script>";
	}
}
$kecamatankode = $_GET['kodekec'];
$edit = mysqli_query($connection, "SELECT *from kecamatan where kecamatanKODE='$kecamatankode'");
$row_edit = mysqli_fetch_array($edit);
$query = mysqli_query($connection, "SELECT * FROM kecamatan");
/**dipanggil data kabupaten */
$squery = mysqli_query($connection, "SELECT * FROM kabupaten");
?>

<section id="main-content">
	<section class="wrapper">
		<div class="row">
			<div class="col-lg-12">
				<h3 class="page-header"><i class="fa fa-laptop"></i> Kecamatan</h3>
				<ol class="breadcrumb">
					<li><i class="fa fa-home"></i><a href="index.php">Home</a></li>
					<li><i class="fa fa-laptop"></i>Kecamatan</li>
				</ol>
			</div>
		</div>
	</section>

	<div class="col-sm-10">
		<form method="POST" class="form-horizontal" enctype="multipart/form-data">
			<div class="form-group form-group-lg">
				<label class="col-sm-3 control-label" for="kecamatanKode">Kode Kecamatan</label>
				<div class="col-sm-6">
					<input class="form-control" type="text" id="kecamatanKode" name="inputKecamatanKode" placeholder="Kode Kecamatan" value="<?php echo $row_edit["kecamatanKODE"] ?>" disabled>
				</div>
			</div>

			<div class="form-group form-group-lg">
				<label class="col-sm-3 control-label" for="KecamatanNama">Nama Kecamatan</label>
				<div class="col-sm-6">
					<input class="form-control" type="text" id="KecamatanNama" name="inputKecamatanNama" placeholder="Nama Kecamatan" value="<?php echo $row_edit["kecamatanNAMA"] ?>">
				</div>
			</div>

			<div class="form-group form-group-lg">
				<label class="col-sm-3 control-label" for="KecamatanAlamat">Alamat Kecamatan</label>
				<div class="col-sm-6">
					<input class="form-control" type="text" id="KecamatanAlamat" name="inputKecamatanAlamat" placeholder="Alamat Kecamatan" value="<?php echo $row_edit["kecamatanALAMAT"] ?>">
				</div>
			</div>

			<div class="form-group form-group-lg">
				<label class="col-sm-3 control-label" for="KecamatanKet">Keterangan Kecamatan</label>
				<div class="col-sm-6">
					<input class="form-control" type="text" id="KecamatanKet" name="inputKecamatanKet" placeholder="Keterangan Kecamatan" value="<?php echo $row_edit["kecamatanKET"] ?>">
				</div>
			</div>

			<div class="form-group form-group-lg">
				<label class="col-sm-3 control-label" for="datepicker">Tanggal Entri</label>
				<div class="col-sm-6">
					<input class="form-control" type="text" id="datepicker" name="kecamatanTGL" placeholder="Tanggal Entri" value="<?php echo $row_edit["kecamatanTGL"] ?>">
				</div>
			</div>

			<!--untuk yang bagian foto -->
			<div class="form-group form-group-lg">
				<label class="col-sm-3 control-label" for="file">Foto Kecamatan</label>
				<img src="images/<?php echo $row_edit['kecamatanFOTO'] ?>" width="80">
				<div class="col-sm-6">
					<input type="file" id="file" name="file">
					<p class="help-block">Untuk Unggah file Kecamatan </p>
				</div>
			</div>
			<!-- untuk bagian input diberi hidden untuk gambarlama supaya pas edit tetap ada-->
			<input type="hidden" name="gambarlama" value="<?= $row_edit["kecamatanFOTO"]; ?>">

			<div class="form-group form-group-lg">
				<label class="col-sm-3 control-label" for="Kabupaten">Nama Kabupaten</label>
				<div class="col-sm-6">
					<select name="kabupatenKODE" class="form-control">
						<option value="kabupaten">Kabupaten</option>
						/**cari isi nya ada tidak di kabupaten */
						<?php if (mysqli_num_rows($squery) > 0) { ?>
							<?php while ($row = mysqli_fetch_array($squery)) { ?>

								<option>

									<?php echo $row["kabupatenKODE"] ?>
									<?php echo $row["kabupatenNAMA"] ?>
								</option>

							<?php } ?>
						<?php } ?>
						</option>

					</select>

				</div>
			</div>

			<div class="col-sm-3">
			</div>
			<div class="col-sm-3">
				<input class="btn btn-lg btn-primary" type="submit" value="Simpan" name="Simpan">
				<!-- tombol diperbesar dg -lg dan berwarna biru dengan -primary -->
				<input class="btn btn-lg btn-info" type="reset" value="Batal"> <!-- tombol berwarna hijau langit -->
			</div>
		</form>
	</div>
</section>





<?php include "footer.php"; ?>