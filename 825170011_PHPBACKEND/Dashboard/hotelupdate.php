<?php include "header.php"; ?>
<?php
include "config.php";
/**mengecek tombol simpan sudah di pilih atau belum */
if (isset($_POST['Simpan'])) {
  if (isset($_REQUEST["inputhotelkode"])) {
    $hotelid = $_REQUEST['inputhotelkode'];
  }
  if (!empty($hotelKODE)) {
    $hotelid = $_POST['inputhotelkode'];
  }
  //tanda petik 1 dan 2 sama
  $hotelid = $_GET['kodehotel'];
  $hotelNAMA = $_POST['inputhotelnama'];
  $hotelKELAS = $_POST['inputhotelkelas'];
  $hotelFASILITAS = $_POST['inputhotelfasilitas'];

  $foto = $_FILES['file']['name'];
  $file_tmp = $_FILES['file']['tmp_name'];
  move_uploaded_file($file_tmp, 'images/' . $foto);

  //untuk menambahkan variabel $gambarlama untuk tidak ingin mengupdate foto lain 
  $gambarlama = $_POST["gambarlama"];
  // cek gambar lama atau baru 
  if ($_FILES['file']['error'] === 4) {
    $foto = $gambarlama;
  }
  //connection harus sama dengan file di config.php
  mysqli_query($connection, "UPDATE hotel SET hotelNAMA='$hotelNAMA',hotelKELAS='$hotelKELAS',hotelFASILITAS='$hotelFASILITAS',hotelFOTO='$foto' where hotelKODE='$hotelid'");
  header("location:indexhotel.php");

  /** untuk menampilkan update berhasil tidak **/
  if (($_POST) > 0) {
    echo "<script>
      alert('data berhasil diupdate!');
      document.location.href= 'hotel.php'
      </script>";
  } else {
    echo "<script>
      alert('data gagal diupdate!');
      document.location.href= 'hotel.php'
      </script>";
  }
}

$hotelid = $_GET['kodehotel'];
$edit = mysqli_query($connection, "SELECT *FROM hotel where hotelKODE='$hotelid'");
$row_edit = mysqli_fetch_array($edit);
$hasilqueri = mysqli_query($connection, "SELECT *FROM hotel");

?>
<section id="main-content">
  <section class="wrapper">
    <div class="row">
      <div class="col-lg-12">
        <h3 class="page-header"><i class="fa fa-laptop"></i> Hotel</h3>
        <ol class="breadcrumb">
          <li><i class="fa fa-home"></i><a href="index.php">Home</a></li>
          <li><i class="fa fa-laptop"></i>Hotel</li>
        </ol>
      </div>
    </div>
  </section>

  <div class="col-sm-10">
    <form method="POST" class="form-horizontal" enctype="multipart/form-data">
      <div class="form-group form-group-lg">
        <label class="col-sm-3 control-label" for="inputhotelkode">Kode Hotel </label>
        <div class="col-sm-6">
          <input class="form-control" type="text" id="inputhotelkode" name="inputhotelkode" placeholder="Kode Foto" value="<?php echo $row_edit["hotelKODE"] ?>" disabled>
        </div>
      </div>

      <div class="form-group form-group-lg">
        <label class="col-sm-3 control-label" for="inputhotelnama">Nama Hotel</label>
        <div class="col-sm-6">
          <input class="form-control" type="text" id="inputhotelnama" name="inputhotelnama" placeholder="Nama Hotel" value="<?php echo $row_edit["hotelNAMA"] ?>">
        </div>
      </div>

      <div class="form-group form-group-lg">
        <label class="col-sm-3 control-label" for="inputhotelkelas">Kelas Hotel</label>
        <div class="col-sm-6">
          <input class="form-control" type="text" id="inputhotelkelas" name="inputhotelkelas" placeholder="Kelas" value="<?php echo $row_edit["hotelKELAS"] ?>">
        </div>
      </div>
      <div class="form-group form-group-lg">
        <label class="col-sm-3 control-label" for="inputhotelfasilitas"> Fasilitas Hotel</label>
        <div class="col-sm-6">
          <input class="form-control" type="text" id="inputhotelfasilitas" name="inputhotelfasilitas" placeholder="fasilitas" value="<?php echo $row_edit["hotelFASILITAS"] ?>">
        </div>
      </div>

      <!-- untuk bagian input diberi hidden untuk gambarlama supaya pas edit tetap ada-->
      <input type="hidden" name="gambarlama" value="<?= $row_edit["hotelFOTO"]; ?>">
      <div class="form-group form-group-lg">
        <label class="col-sm-3 control-label" for="file">Foto Hotel</label>
        <img src="images/<?php echo $row_edit['hotelFOTO'] ?>" width="80">
        <div class="col-sm-6" style="margin-left:7%;">
          <input type="file" id="file" name="file">
          <p class="help-block">Untuk Unggah file foto Gambar </p>
        </div>
      </div>


      <div class="col-sm-3">
      </div>
      <div class="col-sm-3">
        <input class="btn btn-lg btn-primary" type="submit" value="Simpan" name="Simpan">
        <!-- tombol diperbesar dg -lg dan berwarna biru dengan -primary -->
        <input class="btn btn-lg btn-info" type="reset" value="Batal"> <!-- tombol berwarna hijau langit -->
      </div>
    </form>
    <!--dibawah disini-->

  </div>
  <!--penutup col-sm-10-->
</section>
<?php include "footer.php"; ?>