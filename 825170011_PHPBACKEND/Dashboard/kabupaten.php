<?php include "header.php"; ?>
<?php
include "config.php";

if (isset($_POST['Simpan'])) {
  if (isset($_REQUEST["inputkodekabupaten"])) {
    $kabupatenKODE = $_REQUEST['inputkodekabupaten'];
  }
  if (!empty($kabupatenKODE)) {
    $kabupatenKODE = $_POST['inputkodekabupaten'];
  }

  //tanda petik 1 dan 2 sama
  $kabupatenKODE = $_POST['inputkodekabupaten'];
  $kabupatenNAMA = $_POST['inputnamakabupaten'];
  $kabupatenALAMAT = $_POST['inputalamatkabupaten'];
  $kabupatenKET = $_POST['inputketerangankabupaten'];

  $nama = $_FILES['file']['name'];
  $file_tmp = $_FILES['file']['tmp_name'];
  move_uploaded_file($file_tmp, 'images/' . $nama);
  $kabupatenFOTOICONKET = $_POST['inputfotoicon'];

  //connection harus sama dengan file di config.php
  mysqli_query($connection, "INSERT INTO kabupaten VALUES('$kabupatenKODE','$kabupatenNAMA','$kabupatenALAMAT','$kabupatenKET','$nama','$kabupatenFOTOICONKET')");
  header("location:kabupaten.php");
}
$edit = mysqli_query($connection, "SELECT * from kabupaten where kabupatenKODE='$kabupatenKODE'");
$row_edit = mysqli_fetch_array($edit);
/** variabel utk dibagi dan ditampilkan per berapa record **/
$jumlahtampil = 3;
$halaman = @$_GET['page'];
$nomorpage = 1;
if (empty($halaman)) {
  $posisi = 0;
  $halaman = 1;
} else {
  $posisi = ($halaman - 1) * $jumlahtampil;
}
$hasilqueri = mysqli_query($connection, "SELECT * FROM kabupaten limit $posisi,$jumlahtampil");


//searching
// $kab = tsquery("SELECT *from kabupaten order by kabupatenKODE DESC");
// if (isset($_POST['cari'])) {
//   $kab = cari($_POST['keyword']);
// }

?>
<section id="main-content">
  <section class="wrapper">
    <div class="row">
      <div class="col-lg-12">
        <h3 class="page-header"><i class="fa fa-laptop"></i> Kabupaten</h3>
        <ol class="breadcrumb">
          <li><i class="fa fa-home"></i><a href="index.php">Home</a></li>
          <li><i class="fa fa-laptop"></i>Kabupaten</li>
        </ol>
      </div>
    </div>
  </section>


  <!--untuk form-->
  <div class="col-sm-10" style="margin-top:5px;margin-left:5%;">
    <form method="POST" action="" enctype="multipart/form-data">
      <div class="form-group row">
        <label for="kabupatenkode" class="col-sm-4">Kode Kabupaten</label>
        <div class="col-sm-6">
          <input type="text" class="form-control" id="kabupatenkode" name="inputkodekabupaten" placeholder="Kode Kabupaten">
        </div>
        <!--penutup col-sm-6-->
      </div>


      <div class="form-group row">
        <label for="kabupatennama" class="col-sm-4">Nama Kabupaten</label>
        <div class="col-sm-6">
          <input type="text" class="form-control" id="kabupatennama" name="inputnamakabupaten" placeholder="Nama Kabupaten ">
        </div>
        <!--penutup col-sm-6-->
      </div>


      <div class="form-group row">
        <label for="kabupatenalamat" class="col-sm-4">Alamat Kabupaten</label>
        <div class="col-sm-6">
          <input type="text" class="form-control" id="kabupatenalamat" name="inputalamatkabupaten" placeholder="Alamat Kabupaten">

        </div>
      </div>

      <div class="form-group row">
        <label for="kabupatenketerangan" class="col-sm-4">Keterangan Kabupaten</label>
        <div class="col-sm-6">
          <input type="text" class="form-control" id="kabupatenketerangan" name="inputketerangankabupaten" placeholder="Keterangan Kabupaten">
        </div>
      </div>

      <div class="form-group form-group-lg">
        <label class="col-sm-3 control-label" for="file">Foto Kabupaten</label>
        <div class="col-sm-6" style="margin-left:8%;">
          <input type="file" id="file" name="file">
          <p class="help-block">Untuk Unggah file kabupaten </p>
        </div>
      </div>

      <div class="form-group row">
        <label for="kabupatenketerangan" class="col-sm-4">Keterangan Foto Icon</label>
        <div class="col-sm-6">
          <input type="text" class="form-control" id="kabupatenketerangan" name="inputfotoicon" placeholder="Keterangan Kabupaten">
        </div>
      </div>
      <div class="form-group row">
        <div class="col-sm-4">
        </div>
        <div class="col-sm-6">
          <button type="input" name="Simpan" class="btn btn-primary">Simpan</button>
          <button type="button" class="btn btn-success">Batal</button>
        </div>
      </div>


      <!--searching-->
      <form action="" method="post">
        <input type=" text" name="keyword" size="50" autofocus placeholder="Keyword pencarian" autocomplete="off">
        <button type=" submit" name="cari">Cari!</button>
      </form>
      <br>

    </form>
    <!--dibawah disini-->

    <table class="table table-hover">
      <tr class="success">
        <th> No </th>
        <th> Kode Kabupaten </th>
        <th> Nama Kabupaten </th>
        <th> Alamat Kabupaten </th>
        <th> Keterangan Kabupaten </th>
        <th> Foto Icon</th>
        <th>Foto Keterangan</th>
        <th> Action </th>
      </tr>

      <?php
      //apakah table kategori wisata ada atau tidak
      //punya php jadi di tutup yang bagian bawah 
      //milik html yang bagian td maka ditutup dulu 
      //$row array
      if (mysqli_num_rows($hasilqueri) > 0)
        ?>
      <?php $nomor = 1;
    $nomor = 1 + $posisi;
    ?>
      <?php
      while ($row = mysqli_fetch_array($hasilqueri)) {
        ?>
        <!--sesuai nama tabel yang dibawah ini -->
        <tr class="info">
          <td> <?php echo $nomor ?></td>
          <td><?php echo $row['kabupatenKODE']; ?></td>
          <td><?php echo $row['kabupatenNAMA']; ?></td>
          <td> <?php echo $row['kabupatenALAMAT']; ?></td>
          <td><?php echo $row['kabupatenKET']; ?></td>
          <td>
            <img src="images/<?php echo $row['kabupatenFOTOICON'] ?>" width="80">
          </td>
          <td><?php echo $row['kabupatenFOTOICONKET']; ?></td>
          <td>
            <a href="kabupatenupdate.php?kodekab=<?php echo $row["kabupatenKODE"] ?>">EDIT</a>
            <a href="kabupatendelete.php?kodekab=<?php echo $row["kabupatenKODE"] ?>">DELETE</a>
          </td>
        </tr>

        <?php $nomor++; ?>
      <?php
      }  ?>

    </table>
    <?php
    $hasilrecord = mysqli_query($connection, "SELECT *from kabupaten");
    $jumlahrecord = mysqli_num_rows($hasilrecord);
    $jumlahpage = ceil($jumlahrecord / $jumlahtampil);

    ?>
    <!-- PAGINATION untuk bagian pagination   https://getbootstrap.com/docs/4.3/components/pagination/-->
    <nav aria-label="Page navigation example">
      <ul class="pagination">
        <li class="page-item"><a class="page-link" href="?halaman=<?php echo $nomorpage ?>">Pertama</a></li>
        <li class="page-item">
          <?php for ($nomorpage = 1; $nomorpage <= $jumlahpage; $nomorpage++) {
            if ($nomorpage != $halaman) {
              ?><a href="?page=<?php echo $nomorpage ?>"><?php echo $nomorpage; ?></a>
            <?php } else {
                ?><a href="?page=<?php echo $nomorpage ?>"><?php echo $nomorpage; ?></a><?php
                                                                                          }
                                                                                        }
                                                                                        ?>
        </li>
        <li class="page-item"><a class="page-link" href="?page=<?php echo $nomorpage - 1 ?>">Terakhir</a></li>
      </ul>
    </nav>

  </div>
</section>


<?php include "footer.php"; ?>