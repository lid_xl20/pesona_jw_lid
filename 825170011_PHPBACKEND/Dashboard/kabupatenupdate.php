<?php include "header.php"; ?>
<?php
include "config.php";

// $gambarlama = $_FILES['file']['name'];



if (isset($_POST['Simpan'])) {
  if (isset($_REQUEST["inputkodekabupaten"])) {
    $kabupatenKODE = $_REQUEST['inputkodekabupaten'];
  }
  if (!empty($kabupatenKODE)) {
    $kabupatenKODE = $_POST['inputkodekabupaten'];
  }

  //tanda petik 1 dan 2 sama
  $kabupatenKODE = $_GET['kodekab'];
  $kabupatenNAMA = $_POST['inputnamakabupaten'];
  $kabupatenALAMAT = $_POST['inputalamatkabupaten'];
  $kabupatenKET = $_POST['inputketerangankabupaten'];
  // $nama adalah foto nya yang akan diupload ke db
  $nama = $_FILES['file']['name'];
  $file_tmp = $_FILES['file']['tmp_name'];
  move_uploaded_file($file_tmp, 'images/' . $nama);
  $kabupatenFOTOICONKET = $_POST['inputfotoicon'];


  $gambarlama = $_POST["gambarlama"];
  // cek gambar lama atau baru 
  if ($_FILES['file']['error'] === 4) {
    $nama = $gambarlama;
  }

  // $error = $_FILES['file']['error']

  //connection harus sama dengan file di config.php

  mysqli_query($connection, "UPDATE kabupaten set kabupatenNAMA='$kabupatenNAMA',kabupatenALAMAT='$kabupatenALAMAT',kabupatenKET='$kabupatenKET',kabupatenFOTOICON='$nama',kabupatenFOTOICONKET='$kabupatenFOTOICONKET' where kabupatenKODE='$kabupatenKODE'");
  header("location:kabupaten.php");
  /** untuk menampilkan update berhasil tidak **/
  if (($_POST) > 0) {
    echo "<script>
      alert('data berhasil diupdate!');
      document.location.href= 'kabupaten.php'
      </script>";
  } else {
    echo "<script>
      alert('data gagal diupdate!');
      document.location.href= 'kabupaten.php'
      </script>";
  }
}
$kabupatenkode = $_GET['kodekab'];
$edit = mysqli_query($connection, "SELECT * from kabupaten where kabupatenKODE='$kabupatenkode'");
$row_edit = mysqli_fetch_array($edit);
/** variabel utk dibagi dan ditampilkan per berapa record **/
$jumlahtampil = 3;
$halaman = @$_GET['page'];
$nomorpage = 1;
if (empty($halaman)) {
  $posisi = 0;
  $halaman = 1;
} else {
  $posisi = ($halaman - 1) * $jumlahtampil;
}
$hasilqueri = mysqli_query($connection, "SELECT * FROM kabupaten limit $posisi,$jumlahtampil");

?>
<section id="main-content">
  <section class="wrapper">
    <div class="row">
      <div class="col-lg-12">
        <h3 class="page-header"><i class="fa fa-laptop"></i> Kabupaten</h3>
        <ol class="breadcrumb">
          <li><i class="fa fa-home"></i><a href="index.php">Home</a></li>
          <li><i class="fa fa-laptop"></i>Kabupaten</li>
        </ol>
      </div>
    </div>
  </section>

  <!--untuk form-->
  <div class="col-sm-10" style="margin-top:5px;margin-left:5%;">
    <form method="POST" action="" enctype="multipart/form-data">
      <div class="form-group row">
        <label for="kabupatenkode" class="col-sm-4">Kode Kabupaten</label>
        <div class="col-sm-6">
          <input type="text" class="form-control" id="kabupatenkode" name="inputkodekabupaten" placeholder="Kode Kabupaten" value="<?php echo $row_edit["kabupatenKODE"] ?>" disabled>
        </div>
        <!--penutup col-sm-6-->
      </div>


      <div class="form-group row">
        <label for="kabupatennama" class="col-sm-4">Nama Kabupaten</label>
        <div class="col-sm-6">
          <input type="text" class="form-control" id="kabupatennama" name="inputnamakabupaten" placeholder="Nama Kabupaten " value="<?php echo $row_edit["kabupatenNAMA"] ?>">
        </div>
        <!--penutup col-sm-6-->
      </div>


      <div class="form-group row">
        <label for="kabupatenalamat" class="col-sm-4">Alamat Kabupaten</label>
        <div class="col-sm-6">
          <input type="text" class="form-control" id="kabupatenalamat" name="inputalamatkabupaten" placeholder="Alamat Kabupaten" value="<?php echo $row_edit["kabupatenALAMAT"] ?>">
        </div>
      </div>

      <div class="form-group row">
        <label for="kabupatenketerangan" class="col-sm-4">Keterangan Kabupaten</label>
        <div class="col-sm-6">
          <input type="text" class="form-control" id="kabupatenketerangan" name="inputketerangankabupaten" placeholder="Keterangan Kabupaten" value="<?php echo $row_edit["kabupatenKET"] ?>">
        </div>
      </div>

      <div class="form-group form-group-lg">
        <label class="col-sm-3 control-label" for="file">Foto Kabupaten</label>
        <img src="images/<?php echo $row_edit['kabupatenFOTOICON'] ?>" width="80">
        <?php echo $row_edit['kabupatenFOTOICON']; ?>
        <div class="col-sm-6" style="margin-left:8%;">
          <input type="file" id="file" name="file">
          <p class="help-block">Untuk Unggah file kabupaten </p>
        </div>
      </div>
      <!-- untuk menambahkan inputan gambar lama untuk jika tidak ingin ganti foto pas update -->
      <input type="hidden" name="gambarlama" value="<?= $row_edit["kabupatenFOTOICON"]; ?>">

      <div class="form-group row">
        <label for="kabupatenketerangan" class="col-sm-4">Keterangan Foto Icon</label>
        <div class="col-sm-6">
          <input type="text" class="form-control" id="kabupatenketerangan" name="inputfotoicon" placeholder="Keterangan Kabupaten" value="<?php echo $row_edit["kabupatenFOTOICONKET"] ?>">
        </div>
      </div>
      <div class="form-group row">
        <div class="col-sm-4">
        </div>
        <div class="col-sm-6">
          <button type="input" name="Simpan" class="btn btn-primary">Simpan</button>
          <button type="button" class="btn btn-success">Batal</button>
        </div>
      </div>

    </form>
    <!--dibawah disini-->



  </div>
</section>


<?php include "footer.php"; ?>