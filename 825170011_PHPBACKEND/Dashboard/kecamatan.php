<?php include "header.php"; ?>
<?php
include "config.php";
/** Untuk menyisipkan file koneksi ke file config.php **/

/** Mengecek apakah tombol simpan sudah di pilih/klik atau belum **/
     if(isset($_POST['Simpan']))
 	 {	
		if (isset($_REQUEST["inputKecamatanKode"]))
			{	$kecamatanKODE = $_REQUEST["inputKecamatanKode"];
			} 
		if (!empty($kecamatanKODE))
			{	$kecamatanKODE = $_POST['inputKecamatanKode'];	
			}
		$kecamatanKODE=$_POST['inputKecamatanKode'];
		$kecamatanNAMA = $_POST['inputKecamatanNama'];
		$kecamatanALAMAT = $_POST['inputKecamatanAlamat'];
		$kecamatanKET = $_POST['inputKecamatanKet'];
		$kabupatenKODE=$_POST['kabupatenKODE'];
		$kecamatanTANGGAL=$_POST['kecamatanTGL'];

		/**untuk nama file gambar */
			$nama=$_FILES['file']['name'];
			$file_tmp=$_FILES['file']['tmp_name'];
			move_uploaded_file($file_tmp,'images/'.$nama);


/** Memasukkan data ke dalam tabel kecamatan **/
     mysqli_query($connection, "INSERT INTO kecamatan VALUES ('$kecamatanKODE',
	 '$kecamatanNAMA','$kecamatanALAMAT','$kecamatanKET','$kecamatanTANGGAL','$nama','$kabupatenKODE')"); 
     header("location:kecamatan.php");
     }

	 $query = mysqli_query($connection, "SELECT * FROM kecamatan");
	 /**dipanggil data kabupaten */
	 $squery = mysqli_query($connection, "SELECT * FROM kabupaten");
	 /** untuk bagian pages */	

	  /** variabel utk dibagi dan ditampilkan per berapa record **/
	  $jumlahtampil=5;
	  $halaman=@$_GET['page'];
	  $nomorpage = 1;
			if(empty($halaman))
			{
				$posisi=0;
				$halaman=1;
			}
			else
			{
			  $posisi=($halaman-1)*$jumlahtampil;
			}
			/**disamakan dengan yang bagian nama tabel select */
			$query=mysqli_query($connection,"SELECT * FROM kecamatan limit $posisi,$jumlahtampil");
?>	

   <section id="main-content">
      <section class="wrapper">
        <div class="row">
          <div class="col-lg-12">
            <h3 class="page-header"><i class="fa fa-laptop"></i> Kecamatan</h3>
            <ol class="breadcrumb">
              <li><i class="fa fa-home"></i><a href="index.php">Home</a></li>
              <li><i class="fa fa-laptop"></i>Kecamatan</li>
            </ol>
          </div>
        </div>
      </section>
     
	<div class="col-sm-10">
		<form method="POST" class="form-horizontal" enctype="multipart/form-data">
		  <div class="form-group form-group-lg">
			<label class="col-sm-3 control-label" for="kecamatanKode">Kode Kecamatan</label>
			<div class="col-sm-6">
			  <input class="form-control" type="text" id="kecamatanKode" name="inputKecamatanKode" placeholder="Kode Kecamatan" 
			  maxlength="4" required="">
			</div>
		  </div>

		  <div class="form-group form-group-lg">
			<label class="col-sm-3 control-label" for="KecamatanNama">Nama Kecamatan</label>
			<div class="col-sm-6">
			  <input class="form-control" type="text" id="KecamatanNama" name="inputKecamatanNama" placeholder="Nama Kecamatan">
			</div>
		  </div>

		  <div class="form-group form-group-lg">
			<label class="col-sm-3 control-label" for="KecamatanAlamat">Alamat Kecamatan</label>
			<div class="col-sm-6">
			  <input class="form-control" type="text" id="KecamatanAlamat" name="inputKecamatanAlamat" placeholder="Alamat Kecamatan">
			</div>
		  </div>

		  <div class="form-group form-group-lg">
			<label class="col-sm-3 control-label" for="KecamatanKet">Keterangan Kecamatan</label>
			<div class="col-sm-6">
			  <input class="form-control" type="text" id="KecamatanKet" name="inputKecamatanKet" placeholder="Keterangan Kecamatan">
			</div>
		  </div>

      <div class="form-group form-group-lg">
			<label class="col-sm-3 control-label" for="datepicker">Tanggal Entri</label>
			<div class="col-sm-6">
			  <input class="form-control" type="text" id="datepicker" name="kecamatanTGL" placeholder="Tanggal Entri">
			</div>
      </div>
      
			<!--untuk yang bagian foto -->
		  <div class="form-group form-group-lg">
			<label class="col-sm-3 control-label" for="file">Foto Kecamatan</label>
			<div class="col-sm-6">
			  <input type="file" id="file" name="file">
			  <p class="help-block">Untuk Unggah file Kecamatan </p>
			</div>
		  </div>

 
		  <div class="form-group form-group-lg">
			<label class="col-sm-3 control-label" for="Kabupaten">Nama Kabupaten</label>
			<div class="col-sm-6">
	 			<select name="kabupatenKODE"class="form-control">
					 <option value="kabupaten">Kabupaten</option>
						 /**cari isi nya ada tidak di kabupaten */
						 <?php if(mysqli_num_rows($squery)>0) { ?>
							<?php while($row=mysqli_fetch_array($squery)) { ?>

								<option>
								
									<?php echo $row["kabupatenKODE"] ?>
									<?php echo $row["kabupatenNAMA"] ?>
								</option>
							
						 <?php }?>
						 <?php }?>
					 </option>

	 			</select>

				 </div>
		  </div>
		  
		  <div class="col-sm-3">
		  </div>
		  <div class="col-sm-3">
			<input class="btn btn-lg btn-primary" type="submit" value="Simpan" name="Simpan">
			<!-- tombol diperbesar dg -lg dan berwarna biru dengan -primary -->
			<input class="btn btn-lg btn-info" type="reset" value="Batal"> <!-- tombol berwarna hijau langit -->
		  </div>
		</form>	
	
	<table class="table table-hover">
	<!-- membuat judul -->
	<tr class="info">
				<th>NO</th>
				<th>Kode Kecamatan</th>
				<th>Kode Kabupaten</th>
				<th>Nama Kecamatan</th>
				<th>Alamat Kecamatan</th>
				<th>Keterangan Kecamatan</th>
				<th>Tanggal Kecamatan</th>
				<th>Foto Kecamatan</th>
				<th>Kabupaten Kode</th>
				<th>Action</th>
	</tr>
	<?php
		/** Memeriksa apakah data yang dipanggil tersebut tersedia atau tidak **/
		if(mysqli_num_rows($query)>0) 
	{?>
		<?php $no=1; 
			  $no=1+$posisi;
		?>
		<?php while ($row = mysqli_fetch_array($query)) 
			{ ?>
				<tr class="danger">
					<td><?php echo $no; ?></td>
					<td><?php echo $row['kecamatanKODE']; ?> </td>
					<td><?php echo $row['kabupatenKODE']; ?> </td>
					<td><?php echo $row['kecamatanNAMA']; ?> </td>
					<td><?php echo $row['kecamatanALAMAT']; ?> </td>
					<td><?php echo $row['kecamatanKET']; ?> </td>      
					<td><?php echo $row['kecamatanTGL']; ?> </td>  
				<td>
					<img src="images/<?php echo $row['kecamatanFOTO']?>"width="80">

				</td>   
				<td><?php echo $row['kabupatenKODE'];?></td>                        
				<td>
					<a href="kecamatanupdate.php?kodekec=<?php echo $row["kecamatanKODE"]?>">EDIT</a>
					<a href="kecamatandelete.php?kodekec=<?php echo $row["kecamatanKODE"]?>">DELETE</a>
			</td>
				</tr>
				<?php $no++; ?> 
			<?php  } ?>
	<?php  } ?>
	</table>
	<?php
          $hasilrecord=mysqli_query($connection,"SELECT *from kecamatan");
          $jumlahrecord=mysqli_num_rows($hasilrecord);
          $jumlahpage=ceil($jumlahrecord/$jumlahtampil);

          ?>
                <!-- PAGINATION untuk bagian pagination   https://getbootstrap.com/docs/4.3/components/pagination/-->
                <nav aria-label="Page navigation example">
              <ul class="pagination">
                <li class="page-item"><a class="page-link" href="?halaman=<?php echo $nomorpage?>">Pertama</a></li>
                <li class="page-item">
                    <?php for($nomorpage=1;$nomorpage<=$jumlahpage;$nomorpage++)
                    {
                        if($nomorpage!=$halaman)
                        {
                            ?><a href="?page=<?php echo $nomorpage ?>"><?php echo $nomorpage; ?></a>
                        <?php }else
                        {
                            ?><a href="?page=<?php echo $nomorpage ?>"><?php echo $nomorpage; ?></a><?php
                        }
                    }
                    ?>
                </li>
                <li class="page-item"><a class="page-link" href="?page=<?php echo $nomorpage-1 ?>">Terakhir</a></li>
              </ul>
            </nav>

	</div>
    </section>





<?php include "footer.php"; ?>

