<?php include "header.php"; ?>

<?php
include "config.php";
if (isset($_POST['Simpan'])) {
	if (isset($_REQUEST["inputfotoobyekKode"])) {
		$fotoobyekKODE = $_REQUEST["inputfotoobyekKode"];
	}
	if (!empty($fotoobyekKODE)) {
		$fotoobyekKODE = $_POST['inputfotoobyekKode'];
	}

	$fotoobyekKODE = $_POST['fotoobyekKODE'];
	$fotoobyekNAMA = $_POST['inputfotoobyekNama'];
	$obyekKODE = $_POST['obyekKODE'];
	$fotoobyekKET = $_POST['inputfotoobyekKet'];
	$fotoobyektglambil = $_POST['obyektglambil'];


	/**untuk nama file gambar */
	$nama = $_FILES['file']['name'];
	$file_tmp = $_FILES['file']['tmp_name'];
	move_uploaded_file($file_tmp, 'images/' . $nama);

	mysqli_query($connection, "INSERT INTO fotoobyekwisata VALUES ('$fotoobyekKODE','$fotoobyekNAMA','$obyekKODE','$fotoobyekKET','$fotoobyektglambil','$nama')");
	header("location:fotowisatajawa.php");
}

$query = mysqli_query($connection, "SELECT * FROM fotoobyekwisata");
/**dipanggil data kabupaten */
$squery = mysqli_query($connection, "SELECT * FROM obyekwisata");

/** variabel utk dibagi dan ditampilkan per berapa record **/
$jumlahtampil = 3;
$halaman = @$_GET['page'];
$nomorpage = 1;
if (empty($halaman)) {
	$posisi = 0;
	$halaman = 1;
} else {
	$posisi = ($halaman - 1) * $jumlahtampil;
}
$query = mysqli_query($connection, "SELECT * FROM fotoobyekwisata limit $posisi,$jumlahtampil");
?>


<section id="main-content">
	<section class="wrapper">
		<div class="row">
			<div class="col-lg-12">
				<h3 class="page-header"><i class="fa fa-laptop"></i>Foto Wisata</h3>
				<ol class="breadcrumb">
					<li><i class="fa fa-home"></i><a href="index.php">Home</a></li>
					<li><i class="fa fa-laptop"></i>Foto Wisata</li>
				</ol>
			</div>
		</div>
	</section>



	<div class="col-sm-10" style="margin-left:20%;">
		<form method="POST" class="form-horizontal" enctype="multipart/form-data">
			<div class="form-group form-group-lg">
				<label class="col-sm-3 control-label" for="fotoobyekkode">Kode Foto Obyek Wisata</label>
				<div class="col-sm-6">
					<input class="form-control" type="text" id="fotoobyekkode" name="fotoobyekKODE" placeholder="Kode Foto Obyek Wisata" maxlength="4" required="">
				</div>
			</div>

			<div class="form-group form-group-lg">
				<label class="col-sm-3 control-label" for="fotoobyekNama">Nama Foto Obyek</label>
				<div class="col-sm-6">
					<input class="form-control" type="text" id="fotoobyekNama" name="inputfotoobyekNama" placeholder="Nama Foto Obyek Wisata">
				</div>
			</div>

			<div class="form-group form-group-lg">
				<label class="col-sm-3 control-label" for="obyekKODE">Kode Obyek</label>
				<div class="col-sm-6">
					<select name="obyekKODE" class="form-control">
						<option value="obyekKODE">Kategori Obyek Kode</option>
						/**cari isi nya ada tidak di kabupaten */
						<?php if (mysqli_num_rows($squery) > 0) { ?>
							<?php while ($row = mysqli_fetch_array($squery)) { ?>

								<option>

									<?php echo $row["obyekKODE"] ?>
								</option>

							<?php } ?>
						<?php } ?>
						</option>

					</select>

				</div>
			</div>


			<div class="form-group form-group-lg">
				<label class="col-sm-3 control-label" for="fotoobyekKet">Keterangan Obyek Wisata</label>
				<div class="col-sm-6">
					<input class="form-control" type="text" id="fotoobyekKet" name="inputfotoobyekKet" placeholder="Keterangan Obyek Wisata">
				</div>
			</div>

			<div class="form-group form-group-lg">
				<label class="col-sm-3 control-label" for="datepicker">Tanggal Ambil Foto </label>
				<div class="col-sm-6">
					<input class="form-control" type="text" id="datepicker" name="obyektglambil" placeholder="Tanggal Ambil Foto">
				</div>
			</div>
			<!--untuk yang bagian foto -->
			<div class="form-group form-group-lg">
				<label class="col-sm-3 control-label" for="file">Foto Obyek Gambar</label>
				<div class="col-sm-6">
					<input type="file" id="file" name="file">
					<p class="help-block">Untuk Unggah file foto Gambar </p>
				</div>
			</div>

			<div class="col-sm-3">
			</div>
			<div class="col-sm-3">
				<input class="btn btn-lg btn-primary" type="submit" value="Simpan" name="Simpan">
				<!-- tombol diperbesar dg -lg dan berwarna biru dengan -primary -->
				<input class="btn btn-lg btn-info" type="reset" value="Batal"> <!-- tombol berwarna hijau langit -->
			</div>
		</form>

		<table class="table table-hover">
			<!-- membuat judul -->
			<tr class="info">
				<th>NO</th>
				<th>Kode Foto Obyek </th>
				<th>Foto Obyek Nama</th>
				<th>Obyek Kode</th>
				<th>Foto Obyek Keterangan</th>
				<th>Foto Obyek Tanggal Ambil</th>
				<th>Foto Obyek Gambar</th>
				<th>Action</th>
			</tr>
			<?php
			/** Memeriksa apakah data yang dipanggil tersebut tersedia atau tidak **/
			if (mysqli_num_rows($query) > 0) { ?>
				<?php $no = 1;
					$no = 1 + $posisi;
					?>
				<?php while ($row = mysqli_fetch_array($query)) { ?>
					<tr class="danger">
						<td><?php echo $no; ?></td>
						<td><?php echo $row['fotoobyekKODE']; ?> </td>
						<td><?php echo $row['fotoobyekNAMA']; ?> </td>
						<td><?php echo $row['obyekKODE']; ?> </td>
						<td><?php echo $row['fotoobyekKET']; ?> </td>
						<td><?php echo $row['fotoobyekTGLAMBIL']; ?> </td>
						<td>
							<img src="images/<?php echo $row['fotoobyekGAMBAR'] ?>" width="80">

						</td>
						<td>
							<a href="fotowisataupdate.php?kodeobyekfoto=<?php echo $row["fotoobyekKODE"] ?>">EDIT</a>
							<a href="fotowisatadelete.php?kodeobyekfoto=<?php echo $row["fotoobyekKODE"] ?>">DELETE</a>
						</td>
					</tr>
					<?php $no++; ?>
				<?php  } ?>
			<?php  } ?>
		</table>


		<?php
		$hasilrecord = mysqli_query($connection, "SELECT *from fotoobyekwisata");
		$jumlahrecord = mysqli_num_rows($hasilrecord);
		$jumlahpage = ceil($jumlahrecord / $jumlahtampil);

		?>
		<!-- PAGINATION untuk bagian pagination   https://getbootstrap.com/docs/4.3/components/pagination/-->
		<nav aria-label="Page navigation example">
			<ul class="pagination">
				<li class="page-item"><a class="page-link" href="?halaman=<?php echo $nomorpage ?>">Pertama</a></li>
				<li class="page-item">
					<?php for ($nomorpage = 1; $nomorpage <= $jumlahpage; $nomorpage++) {
						if ($nomorpage != $halaman) {
							?><a href="?page=<?php echo $nomorpage ?>"><?php echo $nomorpage; ?></a>
						<?php } else {
								?><a href="?page=<?php echo $nomorpage ?>"><?php echo $nomorpage; ?></a><?php
																											}
																										}
																										?>
				</li>
				<li class="page-item"><a class="page-link" href="?page=<?php echo $nomorpage - 1 ?>">Terakhir</a></li>
			</ul>
		</nav>
	</div>

	<section>







		<?php include "footer.php"; ?>