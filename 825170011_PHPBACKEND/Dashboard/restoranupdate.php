<?php include "header.php"; ?>
<?php
include "config.php";
/**mengecek tombol simpan sudah di pilih atau belum */
if (isset($_POST['Simpan'])) {
  if (isset($_REQUEST["inputrestokode"])) {
    $restokode = $_REQUEST["inputrestokode"];
  }

  if (!empty($restokode)) {
    $restokode = $_POST['inputrestokode'];
  }

  $restokode = $_GET['koderesto'];
  $restobarang = $_POST['inputrestobarang'];
  $restomenu = $_POST['inputrestomenu'];
  $restoharga = $_POST['inputrestoharga'];
  $restotgl = $_POST['inputrestotgl'];

  $foto = $_FILES['file']['name'];
  $file_tmp = $_FILES['file']['tmp_name'];
  move_uploaded_file($file_tmp, 'images/' . $foto);


  //untuk menambahkan variabel $gambarlama untuk tidak ingin mengupdate foto lain 
  $gambarlama = $_POST["gambarlama"];
  // cek gambar lama atau baru 
  if ($_FILES['file']['error'] === 4) {
    $foto = $gambarlama;
  }

  mysqli_query($connection, "UPDATE restoran SET restorankdBarang='$restobarang',restoranMenu='$restomenu',RestoranHargaMenu='$restoharga',RestoranTGL='$restotgl',restoranFOTO='$foto' where restoranKODE='$restokode'");
  header("location:restoran.php");
  /** untuk menampilkan update berhasil tidak **/
  if (($_POST) > 0) {
    echo "<script>
          alert('data berhasil diupdate!');
          document.location.href= 'restoran.php'
          </script>";
  } else {
    echo "<script>
          alert('data gagal diupdate!');
          document.location.href= 'restoran.php'
          </script>";
  }
}


$restokode = $_GET['koderesto'];
$edit = mysqli_query($connection, "SELECT *FROM restoran WHERE restoranKODE='$restokode'");
$row_edit = mysqli_fetch_array($edit);
$hasilqueri = mysqli_query($connection, "SELECT *FROM restoran");
?>

<section id="main-content">
  <section class="wrapper">
    <div class="row">
      <div class="col-lg-12">
        <h3 class="page-header"><i class="fa fa-laptop"></i> Hotel</h3>
        <ol class="breadcrumb">
          <li><i class="fa fa-home"></i><a href="index.php">Home</a></li>
          <li><i class="fa fa-laptop"></i>RESTORAN</li>
        </ol>
      </div>
    </div>
  </section>

  <div class="col-sm-10">
    <form method="POST" class="form-horizontal" enctype="multipart/form-data">
      <div class="form-group form-group-lg">
        <label class="col-sm-3 control-label" for="inputrestokode">Kode Restoran </label>
        <div class="col-sm-6">
          <input class="form-control" type="text" id="inputrestokodee" name="inputrestokode" placeholder="Kode Foto" value="<?php echo $row_edit["restoranKODE"] ?>" disabled>
        </div>
      </div>

      <div class="form-group form-group-lg">
        <label class="col-sm-3 control-label" for="inputrestobarang">Kode Barang</label>
        <div class="col-sm-6">
          <input class="form-control" type="text" id="inputrestobarang" name="inputrestobarang" placeholder="Nama Resto" value="<?php echo $row_edit["restorankdBarang"] ?>">
        </div>
      </div>

      <div class="form-group form-group-lg">
        <label class="col-sm-3 control-label" for="inputrestomenu">Menu Restoran</label>
        <div class="col-sm-6">
          <input class="form-control" type="text" id="inputrestomenu" name="inputrestomenu" placeholder="Menu Resto" value="<?php echo $row_edit["restoranMenu"] ?>">
        </div>
      </div>


      <div class="form-group form-group-lg">
        <label class="col-sm-3 control-label" for="inputrestoharga">Restoran Harga Menu</label>
        <div class="col-sm-6">
          <input class="form-control" type="text" id="inputrestoharga" name="inputrestoharga" placeholder="Harga Menu" value="<?php echo $row_edit["RestoranHargaMenu"] ?>">
        </div>
      </div>

      <div class="form-group form-group-lg">
        <label class="col-sm-3 control-label" for="datepicker">Tanggal Entri</label>
        <div class="col-sm-6">
          <input class="form-control" type="text" id="datepicker" name="inputrestotgl" placeholder="Tanggal Entri" value="<?php echo $row_edit["RestoranTGL"] ?>">
        </div>
      </div>

      <!-- untuk bagian input diberi hidden untuk gambarlama supaya pas edit tetap ada-->
      <input type="hidden" name="gambarlama" value="<?= $row_edit["restoranFOTO"]; ?>">
      <div class="form-group form-group-lg">
        <label class="col-sm-3 control-label" for="file">Foto Restoran </label>
        <img src="images/<?php echo $row_edit['restoranFOTO'] ?>" width="80">
        <div class="col-sm-6" style="margin-left:7%;">
          <input type="file" id="file" name="file">
          <p class="help-block">Untuk Unggah file foto Gambar </p>
        </div>
      </div>


      <div class="col-sm-3">
      </div>
      <div class="col-sm-3">
        <input class="btn btn-lg btn-primary" type="submit" value="Simpan" name="Simpan">
        <!-- tombol diperbesar dg -lg dan berwarna biru dengan -primary -->
        <input class="btn btn-lg btn-info" type="reset" value="Batal"> <!-- tombol berwarna hijau langit -->
      </div>
    </form>


  </div>
</section>

<?php include "footer.php"; ?>