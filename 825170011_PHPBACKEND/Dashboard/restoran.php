<?php include "header.php"; ?>
<?php
include "config.php";
/**mengecek tombol simpan sudah di pilih atau belum */
if (isset($_POST['Simpan'])) {
  if (isset($_REQUEST["inputrestokode"])) {
    $restokode = $_REQUEST["inputrestokode"];
  }

  if (!empty($restokode)) {
    $restokode = $_POST['inputrestokode'];
  }

  $restokode = $_POST['inputrestokode'];
  $restobarang = $_POST['inputrestobarang'];
  $restomenu = $_POST['inputrestomenu'];
  $restoharga = $_POST['inputrestoharga'];
  $restotgl = $_POST['inputrestotgl'];

  $foto = $_FILES['file']['name'];
  $file_tmp = $_FILES['file']['tmp_name'];
  move_uploaded_file($file_tmp, 'images/' . $foto);


  mysqli_query($connection, "INSERT INTO restoran VALUES ('$restokode','$restobarang','$restomenu','$restoharga','$restotgl','$foto')");
  header("location:restoran.php");
}

$hasilqueri = mysqli_query($connection, "SELECT *FROM restoran");


/** variabel utk dibagi dan ditampilkan per berapa record **/
$jumlahtampil = 2;
$halaman = @$_GET['page'];
$nomorpage = 1;
if (empty($halaman)) {
  $posisi = 0;
  $halaman = 1;
} else {
  $posisi = ($halaman - 1) * $jumlahtampil;
}
$hasilqueri = mysqli_query($connection, "SELECT * FROM restoran limit $posisi,$jumlahtampil");

?>

<section id="main-content">
  <section class="wrapper">
    <div class="row">
      <div class="col-lg-12">
        <h3 class="page-header"><i class="fa fa-laptop"></i> Hotel</h3>
        <ol class="breadcrumb">
          <li><i class="fa fa-home"></i><a href="index.php">Home</a></li>
          <li><i class="fa fa-laptop"></i>RESTORAN</li>
        </ol>
      </div>
    </div>
  </section>

  <div class="col-sm-10">
    <form method="POST" class="form-horizontal" enctype="multipart/form-data">
      <div class="form-group form-group-lg">
        <label class="col-sm-3 control-label" for="inputrestokode">Kode Restoran </label>
        <div class="col-sm-6">
          <input class="form-control" type="text" id="inputrestokodee" name="inputrestokode" placeholder="Kode Resto" maxlength="4" required="">
        </div>
      </div>

      <div class="form-group form-group-lg">
        <label class="col-sm-3 control-label" for="inputrestobarang">Kode Barang</label>
        <div class="col-sm-6">
          <input class="form-control" type="text" id="inputrestobarang" name="inputrestobarang" placeholder="Nama Resto">
        </div>
      </div>

      <div class="form-group form-group-lg">
        <label class="col-sm-3 control-label" for="inputrestomenu">Menu Restoran</label>
        <div class="col-sm-6">
          <input class="form-control" type="text" id="inputrestomenu" name="inputrestomenu" placeholder="Nama Resto">
        </div>
      </div>


      <div class="form-group form-group-lg">
        <label class="col-sm-3 control-label" for="inputrestoharga">Restoran Harga Menu</label>
        <div class="col-sm-6">
          <input class="form-control" type="text" id="inputrestoharga" name="inputrestoharga" placeholder="Harga Menu">
        </div>
      </div>

      <div class="form-group form-group-lg">
        <label class="col-sm-3 control-label" for="datepicker">Tanggal Entri</label>
        <div class="col-sm-6">
          <input class="form-control" type="text" id="datepicker" name="inputrestotgl" placeholder="Tanggal Entri">
        </div>
      </div>

      <div class="form-group form-group-lg">
        <label class="col-sm-3 control-label" for="file">Foto Restoran </label>
        <div class="col-sm-6" style="margin-left:7%;">
          <input type="file" id="file" name="file">
          <p class="help-block">Untuk Unggah file foto Gambar </p>
        </div>
      </div>


      <div class="col-sm-3">
      </div>
      <div class="col-sm-3">
        <input class="btn btn-lg btn-primary" type="submit" value="Simpan" name="Simpan">
        <!-- tombol diperbesar dg -lg dan berwarna biru dengan -primary -->
        <input class="btn btn-lg btn-info" type="reset" value="Batal"> <!-- tombol berwarna hijau langit -->
      </div>
    </form>




    <table class="table table-hover">
      <tr class="success" style="margin-left:100px;">
        <th> No </th>
        <th> Kode Restoran </th>
        <th>Nama Barang Restoran</th>
        <th> Menu Restoran</th>
        <th> Harga Menu </th>
        <th> Tanggal </th>
        <th> Restoran Foto </th>
        <th> Action </th>
      </tr>



      <?php

      if (mysqli_num_rows($hasilqueri) > 0) { ?>
        <?php $nomor = 1;
          $nomor = 1 + $posisi;

          ?>
        <?php while ($row = mysqli_fetch_array($hasilqueri)) { ?>
          <!--sesuai nama tabel yang dibawah ini -->
          <tr class="danger">
            <td> <?php echo $nomor; ?></td>
            <td><?php echo $row['restoranKODE']; ?></td>
            <td><?php echo $row['restorankdBarang']; ?></td>
            <td><?php echo $row['restoranMenu']; ?></td>
            <td><?php echo $row['RestoranHargaMenu']; ?></td>
            <td><?php echo $row['RestoranTGL']; ?></td>
            <td>
              <img src="images/<?php echo $row['restoranFOTO'] ?>" width="80">
            </td>
            <td>
              <a href="restoranupdate.php?koderesto=<?php echo $row["restoranKODE"] ?>">EDIT||</a>
              <a href="restorandelete.php?koderesto=<?php echo $row["restoranKODE"] ?>">DELETE</a>
            </td>
          </tr>
          <?php $nomor++; ?>
        <?php  } ?>
      <?php  } ?>
    </table>
    <?php
    $hasilrecord = mysqli_query($connection, "SELECT *from restoran");
    $jumlahrecord = mysqli_num_rows($hasilrecord);
    $jumlahpage = ceil($jumlahrecord / $jumlahtampil);

    ?>
    <!-- PAGINATION untuk bagian pagination   https://getbootstrap.com/docs/4.3/components/pagination/-->
    <nav aria-label="Page navigation example">
      <ul class="pagination">
        <li class="page-item"><a class="page-link" href="?halaman=<?php echo $nomorpage ?>">Pertama</a></li>
        <li class="page-item">
          <?php for ($nomorpage = 1; $nomorpage <= $jumlahpage; $nomorpage++) {
            if ($nomorpage != $halaman) {
              ?><a href="?page=<?php echo $nomorpage ?>"><?php echo $nomorpage; ?></a>
            <?php } else {
                ?><a href="?page=<?php echo $nomorpage ?>"><?php echo $nomorpage; ?></a><?php
                                                                                          }
                                                                                        }
                                                                                        ?>
        </li>
        <li class="page-item"><a class="page-link" href="?page=<?php echo $nomorpage - 1 ?>">Terakhir</a></li>
      </ul>
    </nav>
  </div>
</section>


<?php include "footer.php"; ?>