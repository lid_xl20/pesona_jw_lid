<?php include "header.php"; ?>

<?php
include "config.php";
if (isset($_POST['Simpan'])) {
	if (isset($_REQUEST["inputfotoobyekKode"])) {
		$fotoobyekKODE = $_REQUEST["inputfotoobyekKode"];
	}
	if (!empty($fotoobyekKODE)) {
		$fotoobyekKODE = $_POST['inputfotoobyekKode'];
	}

	$fotoobyekKODE = $_GET['kodeobyekfoto'];
	$fotoobyekNAMA = $_POST['inputfotoobyekNama'];
	$obyekKODE = $_POST['obyekKODE'];
	$fotoobyekKET = $_POST['inputfotoobyekKet'];
	$fotoobyektglambil = $_POST['obyektglambil'];


	/**untuk nama file gambar */
	$nama = $_FILES['file']['name']; //untuk mendapatkan nama file yang diupload
	$file_tmp = $_FILES['file']['tmp_name']; //untuk temporary file yang diupload
	move_uploaded_file($file_tmp, 'images/' . $nama);

	//untuk menambahkan variabel $gambarlama untuk tidak ingin mengupdate foto lain 
	$gambarlama = $_POST["gambarlama"];
	// cek gambar lama atau baru 
	//cek apakah user pilih gambar baru/tidak
	//tampilkan pesan error
	//user tidak upload gambar apa2 maka masuk $nama
	if ($_FILES['file']['error'] === 4) {
		$nama = $gambarlama;
	} //jika ada gambar maka error =0
	mysqli_query($connection, "UPDATE fotoobyekwisata SET fotoobyekNAMA='$fotoobyekNAMA',fotoobyekKET='$fotoobyekKET',fotoobyekTGLAMBIL='$fotoobyektglambil',fotoobyekGAMBAR='$nama'
    where fotoobyekKODE='$fotoobyekKODE'");
	header("location:fotowisatajawa.php");
	/** untuk menampilkan update berhasil tidak **/
	if (($_POST) > 0) {
		echo "<script>
      alert('data berhasil diupdate!');
      document.location.href= 'fotowisatajawa.php'
      </script>";
	} else {
		echo "<script>
      alert('data gagal diupdate!');
      document.location.href= 'fotowisatajawa.php'
      </script>";
	}
}

$fotoobyekkode = $_GET['kodeobyekfoto'];
$edit = mysqli_query($connection, "SELECT * FROM fotoobyekwisata where fotoobyekKODE='$fotoobyekkode'");
$row_edit = mysqli_fetch_array($edit);

$query = mysqli_query($connection, "SELECT * FROM fotoobyekwisata");
/**dipanggil data kabupaten */
$squery = mysqli_query($connection, "SELECT * FROM obyekwisata");
?>


<section id="main-content">
	<section class="wrapper">
		<div class="row">
			<div class="col-lg-12">
				<h3 class="page-header"><i class="fa fa-laptop"></i>Foto Wisata</h3>
				<ol class="breadcrumb">
					<li><i class="fa fa-home"></i><a href="index.php">Home</a></li>
					<li><i class="fa fa-laptop"></i>Foto Wisata</li>
				</ol>
			</div>
		</div>
	</section>



	<div class="col-sm-10" style="margin-left:20%;">
		<form method="POST" class="form-horizontal" enctype="multipart/form-data">
			<div class="form-group form-group-lg">
				<label class="col-sm-3 control-label" for="fotoobyekkode">Kode Foto Obyek Wisata</label>
				<div class="col-sm-6">
					<input class="form-control" type="text" id="fotoobyekkode" name="fotoobyekKODE" placeholder="Kode Foto Obyek Wisata" value="<?php echo $row_edit["fotoobyekKODE"] ?>" disabled>
				</div>
			</div>

			<div class="form-group form-group-lg">
				<label class="col-sm-3 control-label" for="fotoobyekNama">Nama Foto Obyek</label>
				<div class="col-sm-6">
					<input class="form-control" type="text" id="fotoobyekNama" name="inputfotoobyekNama" placeholder="Nama Foto Obyek Wisata" value="<?php echo $row_edit["fotoobyekNAMA"] ?>">
				</div>
			</div>

			<div class="form-group form-group-lg">
				<label class="col-sm-3 control-label" for="obyekKODE">Kode Obyek</label>
				<div class="col-sm-6">
					<select name="obyekKODE" class="form-control">
						<option value="<?php echo $row_edit["obyekKODE"] ?>">Kategori Obyek Kode</option>
						/**cari isi nya ada tidak di kabupaten */
						<?php if (mysqli_num_rows($squery) > 0) { ?>
							<?php while ($row = mysqli_fetch_array($squery)) { ?>

								<option>

									<?php echo $row["obyekKODE"] ?>
								</option>

							<?php } ?>
						<?php } ?>
						</option>

					</select>

				</div>
			</div>

			<div class="form-group form-group-lg">
				<label class="col-sm-3 control-label" for="fotoobyekKet">Keterangan Obyek Wisata</label>
				<div class="col-sm-6">
					<input class="form-control" type="text" id="fotoobyekKet" name="inputfotoobyekKet" placeholder="Keterangan Obyek Wisata" value="<?php echo $row_edit["fotoobyekKET"] ?>">
				</div>
			</div>

			<div class="form-group form-group-lg">
				<label class="col-sm-3 control-label" for="datepicker">Tanggal Ambil Foto </label>
				<div class="col-sm-6">
					<input class="form-control" type="text" id="datepicker" name="obyektglambil" placeholder="Tanggal Ambil Foto" value="<?php echo $row_edit["fotoobyekTGLAMBIL"] ?>">
				</div>
			</div>
			<!--untuk yang bagian foto -->
			<!-- untuk bagian input diberi hidden untuk gambarlama supaya pas edit tetap ada-->
			<input type="hidden" name="gambarlama" value="<?= $row_edit["fotoobyekGAMBAR"]; ?>">
			<div class="form-group form-group-lg">
				<img src="images/<?php echo $row_edit['fotoobyekGAMBAR'] ?>" width="80">
				<label class="col-sm-3 control-label" for="file">Foto Obyek Gambar</label>
				<div class="col-sm-6">
					<input type="file" id="file" name="file">
					<p class="help-block">Untuk Unggah file foto Gambar </p>
				</div>
			</div>

			<div class="col-sm-3">
			</div>
			<div class="col-sm-3">
				<input class="btn btn-lg btn-primary" type="submit" value="Simpan" name="Simpan">
				<!-- tombol diperbesar dg -lg dan berwarna biru dengan -primary -->
				<input class="btn btn-lg btn-info" type="reset" value="Batal"> <!-- tombol berwarna hijau langit -->
			</div>
		</form>
	</div>

	<section>

		<?php include "footer.php"; ?>