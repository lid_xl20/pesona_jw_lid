<?php include "header.php";

?>
<?php
include "config.php";
if (isset($_POST['Simpan'])) {
  if (isset($_REQUEST["inputberitaKode"])) {
    $beritaKODE = $_REQUEST["inputberitaKode"];
  }
  if (!empty($beritaKODE)) {
    $beritaKODE = $_POST['inputberitaKode'];
  }
  $beritaKODE = $_GET['kodeberita'];
  $beritaJUDUL = $_POST['inputberitaJudul'];
  $kategoriberitaKODE = $_POST['inputkategoriberitaKode'];
  $eventKODE = $_POST['inputeventKode'];
  $kabupatenKODE = $_POST['inputkabupatenKode'];
  $beritaISI = $_POST['inputberitaIsi'];
  $beritaISI2 = $_POST['inputberitaIsi2'];
  $beritaSUMBER = $_POST['inputberitaSumber'];
  $beritaPENULIS = $_POST['inputberitaPenulis'];
  $beritaTGL = $_POST['inputberitaTgl'];

  $nama = $_FILES['file']['name'];
  $file_tmp = $_FILES['file']['tmp_name'];
  move_uploaded_file($file_tmp, 'images/' . $nama);


  //untuk menambahkan variabel $gambarlama untuk tidak ingin mengupdate foto lain 
  $gambarlama = $_POST["gambarlama"];
  // cek gambar lama atau baru 
  if ($_FILES['file']['error'] === 4) {
    $nama = $gambarlama;
  }
  /** Memasukkan data fullname ke dalam tabel berita **/
  mysqli_query($connection, "UPDATE berita SET beritaJUDUL='$beritaJUDUL',kategoriberitaKODE='$kategoriberitaKODE',eventKODE='$eventKODE',
     kabupatenKODE='$kabupatenKODE',beritaISI='$beritaISI',beritaISI2='$beritaISI2',beritaSUMBER='$beritaSUMBER',
     beritaPENULIS='$beritaPENULIS',beritaTGL='$beritaTGL',beritaICONFOTO='$nama' where beritaKODE='$beritaKODE'");
  header("location:isiberita.php");

  /** untuk menampilkan update berhasil tidak **/
  if (($_POST) > 0) {
    echo "<script>
          alert('data berhasil diupdate!');
          document.location.href= 'isiberita.php'
          </script>";
  } else {
    echo "<script>
          alert('data gagal diupdate!');
          document.location.href= 'isiberita.php'
          </script>";
  }
}


$beritakode = $_GET['kodeberita'];
$edit = mysqli_query($connection, "SELECT *FROM berita where beritaKODE='$beritakode'");
$row_edit = mysqli_fetch_array($edit);
$kategoriqueri = mysqli_query($connection, "select * FROM kategoriberita");
$kabupatenqueri = mysqli_query($connection, "select * FROM kabupaten");


?>


<section id="main-content">
  <section class="wrapper">
    <div class="row">
      <div class="col-lg-12">
        <h3 class="page-header"><i class="fa fa-laptop"></i> Isi Berita</h3>
        <ol class="breadcrumb">
          <li><i class="fa fa-home"></i><a href="index.php">Home</a></li>
          <li><i class="fa fa-laptop"></i>Isi Berita</li>
        </ol>
      </div>
    </div>
  </section>

  <div class="col-sm-15">
    <form method="POST" class="form-horizontal" enctype="multipart/form-data">
      <div class="form-group form-group-lg">
        <label class="col-sm-3 control-label" for="beritaKode">Kode berita</label>
        <div class="col-sm-6">
          <input class="form-control" type="text" id="beritaKode" name="inputberitaKode" placeholder="Kode berita" value="<?php echo $row_edit["beritaKODE"] ?>" disabled>
        </div>
      </div>

      <div class="form-group form-group-lg">
        <label class="col-sm-3 control-label" for="beritaJUDUL">Nama berita</label>
        <div class="col-sm-6">
          <input class="form-control" type="text" id="beritaJUDUL" name="inputberitaJudul" placeholder="Nama berita" value="<?php echo $row_edit["beritaJUDUL"] ?>">
        </div>
      </div>

      <div class="form-group form-group-lg">
        <label class="col-sm-3 control-label" for="kategoriberitakode">Nama Kategori</label>
        <div class="col-sm-6">
          <select name="inputkategoriberitaKode" class="form-control">
            <option value="<?php echo $row_edit["kategoriberitaKODE"] ?>">Kategori</option>
            /**cari isi nya ada tidak di kabupaten */
            <?php if (mysqli_num_rows($kategoriqueri) > 0) { ?>
              <?php while ($row = mysqli_fetch_array($kategoriqueri)) { ?>

                <option>

                  <?php echo $row["kategoriberitaKODE"] ?>
                  <?php echo $row["kategoriberitaNAMA"] ?>
                </option>

              <?php } ?>
            <?php } ?>
            </option>

          </select>

        </div>
      </div>


      <div class="form-group form-group-lg">
        <label class="col-sm-3 control-label" for="event">Nama Event</label>
        <div class="col-sm-6">
          <input class="form-control" type="text" id="event" name="inputeventKode" placeholder="Nama Event" value="<?php echo $row_edit["eventKODE"] ?>">
        </div>
      </div>


      <div class="form-group form-group-lg">
        <label class="col-sm-3 control-label" for="Kabupaten">Nama Kabupaten</label>
        <div class="col-sm-6">
          <select name="inputkabupatenKode" class="form-control">
            <option value="<?php echo $row_edit["kabupatenKODE"] ?>">Kabupaten</option>
            /**cari isi nya ada tidak di kabupaten */
            <?php if (mysqli_num_rows($kabupatenqueri) > 0) { ?>
              <?php while ($row = mysqli_fetch_array($kabupatenqueri)) { ?>

                <option>

                  <?php echo $row["kabupatenKODE"] ?>
                  <?php echo $row["kabupatenNAMA"] ?>
                </option>

              <?php } ?>
            <?php } ?>
            </option>

          </select>

        </div>
      </div>

      <div class="form-group form-group-lg">
        <label class="col-sm-3 control-label" for="beritaISI">Isi berita 1</label>
        <div class="col-sm-6">
          <input class="form-control" type="text" id="beritaISI" name="inputberitaIsi" placeholder="Isi berita" value="<?php echo $row_edit["beritaISI"] ?>">
        </div>
      </div>

      <div class="form-group form-group-lg">
        <label class="col-sm-3 control-label" for="beritaISI2">Isi berita 2</label>
        <div class="col-sm-6">
          <input class="form-control" type="text" id="beritaISI2" name="inputberitaIsi2" placeholder="Isi berita 2" value="<?php echo $row_edit["beritaISI2"] ?>">
        </div>
      </div>

      <div class="form-group form-group-lg">
        <label class="col-sm-3 control-label" for="sumber">Sumber</label>
        <div class="col-sm-6">
          <input class="form-control" type="text" id="sumber" name="inputberitaSumber" placeholder="Sumber berita" value="<?php echo $row_edit["beritaSUMBER"] ?>">
        </div>
      </div>

      <div class="form-group form-group-lg">
        <label class="col-sm-3 control-label" for="penulis">Penulis</label>
        <div class="col-sm-6">
          <input class="form-control" type="text" id="penulis" name="inputberitaPenulis" placeholder="Penulis berita" value="<?php echo $row_edit["beritaPENULIS"] ?>">
        </div>
      </div>

      <div class="form-group form-group-lg">
        <label class="col-sm-3 control-label" for="datepicker">Tanggal Entri</label>
        <div class="col-sm-6">
          <input class="form-control" type="text" id="datepicker" name="inputberitaTgl" placeholder="Tanggal Entri" value="<?php echo $row_edit["beritaTGL"] ?>">
        </div>
      </div>


      <!-- untuk bagian input diberi hidden untuk gambarlama supaya pas edit tetap ada-->
      <input type="hidden" name="gambarlama" value="<?= $row_edit["beritaICONFOTO"]; ?>">

      <div class="form-group form-group-lg">
        <label class="col-sm-3 control-label" for="file">Foto berita</label>
        <img src="images/<?php echo $row_edit['beritaICONFOTO']; ?>" width="80">
        <div class="col-sm-6">
          <input type="file" id="file" name="file">
          <p class="help-block"> Untuk Unggah File berita</p>
        </div>
      </div>

      <div class="col-sm-3">
      </div>
      <div class="col-sm-3">
        <input class="btn btn-lg btn-primary" type="submit" value="Simpan" name="Simpan">
        <!-- tombol diperbesar dg -lg dan berwarna biru dengan -primary -->
        <input class="btn btn-lg btn-info" type="reset" value="Batal"> <!-- tombol berwarna hijau langit -->
      </div>
    </form>

  </div>
</section>

<?php include "footer.php"; ?>