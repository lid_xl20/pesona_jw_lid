<?php include "header.php";
?>

<?php
include "config.php";
if (isset($_POST['Simpan'])) {
	if (isset($_REQUEST["inputprovkode"])) {
		$provinsiKODE = $_REQUEST['inputprovkode'];
	}
	if (!empty($provinsiKODE)) {
		$provinsiKODE = $_POST['inputprovkode'];
	}

	$provinsiKODE = $_POST['inputprovkode'];
	$provinsiNAMA = $_POST['inputprovnama'];
	$kabupatenKODE = $_POST['inputkabupatenkode'];
	$kecamatanKODE = $_POST['inputkecamatankode'];
	$ibukotaPROVINSI = $_POST['inputibukotaprovinsi'];
	$luasWILAYAH = $_POST['inputluaswilayah'];

	/** untuk foto */
	$nama = $_FILES['file']['name'];
	$file_tmp = $_FILES['file']['tmp_name'];
	move_uploaded_file($file_tmp, 'images/' . $nama);

	mysqli_query($connection, "INSERT INTO provinsi VALUES('$provinsiKODE','$provinsiNAMA','$kabupatenKODE','$kecamatanKODE','$ibukotaPROVINSI','$luasWILAYAH','$nama')");
	header("location:provinsi.php");
}

$query = mysqli_query($connection, "SELECT *FROM provinsi");
$squery = mysqli_query($connection, "SELECT *FROM kabupaten");
$tquery = mysqli_query($connection, "SELECT *FROM kecamatan");


/** variabel utk dibagi dan ditampilkan per berapa record **/
$jumlahtampil = 2;
$halaman = @$_GET['page'];
$nomorpage = 1;
if (empty($halaman)) {
	$posisi = 0;
	$halaman = 1;
} else {
	$posisi = ($halaman - 1) * $jumlahtampil;
}
$query = mysqli_query($connection, "SELECT * FROM provinsi limit $posisi,$jumlahtampil");
?>

<section id="main-content">
	<section class="wrapper">
		<div class="row">
			<div class="col-lg-12">
				<h3 class="page-header"><i class="fa fa-laptop"></i> Provinsi</h3>
				<ol class="breadcrumb">
					<li><i class="fa fa-home"></i><a href="index.php">Home</a></li>
					<li><i class="fa fa-laptop"></i>Provinsi</li>
				</ol>
			</div>
		</div>
	</section>

	<div class="col-sm-10">
		<form method="POST" action="" class="form-horizontal" enctype="multipart/form-data">
			<div class="form-group form-group-lg">
				<label class="col-sm-3 control-label" for="inputprovkode">Provinsi Kode </label>
				<div class="col-sm-6">
					<input class="form-control" type="text" id="inputprovkode" name="inputprovkode" placeholder="Kode Prov" maxlength="4" required="">
				</div>
			</div>

			<div class="form-group form-group-lg">
				<label class="col-sm-3 control-label" for="inputprovnama">Nama Provinsi</label>
				<div class="col-sm-6">
					<input class="form-control" type="text" id="inputprovnama" name="inputprovnama" placeholder="NamaProv">
				</div>
			</div>

			<div class="form-group form-group-lg">
				<label class="col-sm-3 control-label" for="inputkabupatenkode">Kode Kabupaten</label>
				<div class="col-sm-6">
					<select name="inputkabupatenkode" class="form-control">
						<option value="kabupaten">Kabupaten Kode</option>
						<?php if (mysqli_num_rows($squery) > 0) { ?>
							<?php while ($row = mysqli_fetch_array($squery)) { ?>
								<option>
									<?php echo $row["kabupatenKODE"] ?>
									<?php echo $row["kabupatenNAMA"] ?>
								</option>
							<?php } ?>
						<?php } ?>
						</option>
					</select>
				</div>
			</div>

			<div class="form-group form-group-lg">
				<label class="col-sm-3 control-label" for="inputkecamatankode">Kode Kecamatan Kategori</label>
				<div class="col-sm-6">
					<select name="inputkecamatankode" class="form-control">
						<option value="kecamatan">Kategori Kecamatan Kode</option>
						<?php if (mysqli_num_rows($tquery) > 0) { ?>
							<?php while ($row = mysqli_fetch_array($tquery)) { ?>
								<option>
									<?php echo $row["kecamatanKODE"] ?>
									<?php echo $row["kecamatanNAMA"] ?>
								</option>
							<?php } ?>
						<?php } ?>
						</option>
					</select>
				</div>
			</div>

			<div class="form-group form-group-lg">
				<label class="col-sm-3 control-label" for="inputibukotaprovinsi">Nama Ibukota</label>
				<div class="col-sm-6">
					<input class="form-control" type="text" id="inputibukotaprovinsi" name="inputibukotaprovinsi" placeholder="Nama Ibukota">
				</div>
			</div>

			<div class="form-group form-group-lg">
				<label class="col-sm-3 control-label" for="inputluaswilayah">Luas Wilayah</label>
				<div class="col-sm-6">
					<input class="form-control" type="text" id="inputluaswilayah" name="inputluaswilayah" placeholder="Nama Luas">
				</div>
			</div>

			<div class="form-group form-group-lg">
				<label class="col-sm-3 control-label" for="file">FotoProvinsi</label>
				<div class="col-sm-6">
					<input type="file" id="file" name="file">
					<p class="help-block">Untuk Unggah file Provinsi</p>
				</div>
			</div>

			<div class="col-sm-3">
			</div>
			<div class="col-sm-3">
				<input class="btn btn-lg btn-primary" type="submit" value="Simpan" name="Simpan">
				<!-- tombol diperbesar dg -lg dan berwarna biru dengan -primary -->
				<input class="btn btn-lg btn-info" type="reset" value="Batal">
				<!-- tombol berwarna hijau langit -->
			</div>

		</form>



		<table class="table table-hover">
			<!-- membuat judul -->
			<tr class="info">
				<th>NO</th>
				<th>Kode Provinsi</th>
				<th>Provinsi Nama</th>
				<th>Kabupaten Kode</th>
				<th>Kecamatan Kode</th>
				<th>Ibukota Provinsi</th>
				<th>Luas Wilayah</th>
				<th>Foto Provinsi </th>
				<th> Action</th>

			</tr>


			<?php
			/** Memeriksa apakah data yang dipanggil tersebut tersedia atau tidak **/
			if (mysqli_num_rows($query) > 0) { ?>
				<?php $no = 1;
					$no = 1 + $posisi;
					?>
				<?php while ($row = mysqli_fetch_array($query)) { ?>
					<tr class="danger">
						<!--sama dengan nama tabel-->
						<td><?php echo $no; ?></td>
						<td><?php echo $row['provinsiKODE']; ?> </td>
						<td><?php echo $row['provinsiNAMA']; ?> </td>
						<td><?php echo $row['kabupatenKODE']; ?> </td>
						<td><?php echo $row['kecamatanKODE']; ?> </td>
						<td><?php echo $row['ibukotaPROV']; ?> </td>
						<td><?php echo $row['luasWILAYAH']; ?> </td>
						<td>
							<img src="images/<?php echo $row['provinsiFOTO'] ?>" width="80">
						</td>
						<td>
							<a href="provinsiupdate.php?kodeprovinsi=<?php echo $row["provinsiKODE"] ?>">EDIT||</a>
							<a href="provinsidelete.php?kodeprovinsi=<?php echo $row["provinsiKODE"] ?>">DELETE</a>
						</td>
					</tr>

					<?php $no++; ?>
				<?php  } ?>
			<?php  } ?>
		</table>

		<?php
		$hasilrecord = mysqli_query($connection, "SELECT *from provinsi");
		$jumlahrecord = mysqli_num_rows($hasilrecord);
		$jumlahpage = ceil($jumlahrecord / $jumlahtampil);

		?>
		<!-- PAGINATION untuk bagian pagination   https://getbootstrap.com/docs/4.3/components/pagination/-->
		<nav aria-label="Page navigation example">
			<ul class="pagination">
				<li class="page-item"><a class="page-link" href="?halaman=<?php echo $nomorpage ?>">Pertama</a></li>
				<li class="page-item">
					<?php for ($nomorpage = 1; $nomorpage <= $jumlahpage; $nomorpage++) {
						if ($nomorpage != $halaman) {
							?><a href="?page=<?php echo $nomorpage ?>"><?php echo $nomorpage; ?></a>
						<?php } else {
								?><a href="?page=<?php echo $nomorpage ?>"><?php echo $nomorpage; ?></a><?php
																											}
																										}
																										?>
				</li>
				<li class="page-item"><a class="page-link" href="?page=<?php echo $nomorpage - 1 ?>">Terakhir</a></li>
			</ul>
		</nav>












	</div>
</section>

<?php include "footer.php"; ?>